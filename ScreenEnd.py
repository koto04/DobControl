from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.button import Button
from kivy.uix.label import Label

from threading import Timer

class ScreenEnd(Screen):

    buttonTube = None
    buttonOAZ = None
    buttonTelrad = None
    labelEnd = None
    buttonEnd = ObjectProperty()
    
    def __init__(self, **kwargs):
        super(ScreenEnd, self).__init__(**kwargs)
        self.buttonTube = ToggleButton(text='Close Tube', on_release=self.checkEnd)
        self.buttonOAZ = ToggleButton(text='Close OAZ', on_release=self.checkEnd)
        self.buttonTelrad = ToggleButton(text='Switch off Telrad', on_release=self.checkEnd)
        self.buttonEnd = Button(text='Back', on_release=self.end)
        self.labelEnd = Label(text='See you!')

        self.check_items.add_widget(self.buttonTube)
        self.check_items.add_widget(self.buttonOAZ)
        self.check_items.add_widget(self.buttonTelrad)
        self.check_items.add_widget(self.buttonEnd)

    def on_enter(self, *args):

        Screen.on_enter(self, *args)
        self.buttonTube.state="normal"
        self.buttonOAZ.state="normal"
        self.buttonTelrad.state="normal"
                
    def checkEnd(self, *args):
        if self.buttonTube.state=="down" and self.buttonOAZ.state=="down" and self.buttonTelrad.state=="down":
            self.buttonEnd.text = "Shoot down"
        else:
            self.buttonEnd.text = "Back"
        
    def end(self, *args):
        if self.buttonTube.state=="down" and self.buttonOAZ.state=="down" and self.buttonTelrad.state=="down":
            #os.system("sudo shutdown -h now")
            self.check_items.clear_widgets()
            self.check_items.add_widget(self.labelEnd)
            et = Timer(3, self.app.stop)
            et.start()
        else:
            self.manager.current='screenMain'
