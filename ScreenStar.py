'''
Created on 29.01.2015

@author: martin
'''

from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.modalview import ModalView
from kivy.uix.image import Image

from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from kivy.clock import Clock
from KoordCalculation import KoordCalculation
from Nunchuk import Nunchuk

from CatalogObject import CatalogObject
import sys, os
import threading
import time

class PopupCalibrateStar(ModalView):
    
    screenmanager = ObjectProperty()
    calculator = ObjectProperty(KoordCalculation)
    app = ObjectProperty()
    
    calibrationTitle = StringProperty()
    calibrationText = StringProperty()
    
    rek = NumericProperty()
    dek = NumericProperty()

    object_id = ''

    nunchuk = None
    control = None
    monitorInput = False
    
    
    def on_open(self):
        ModalView.on_open(self)
        if(os.path.isfile(os.path.dirname(os.path.abspath(__file__)) + "/objects/" + self.object_id + ".jpg")):
            obj_image = Image(source=os.path.dirname(os.path.abspath(__file__)) + "/objects/" + self.object_id + ".jpg", keep_ratio=True, size_hint=(0.95, 0.95), pos_hint={'x': 0.05, 'y': 0.05})
            if(self.app.nightMode==True):
                obj_image.color = [1,0,0,1]
            
            self.object_image.add_widget(obj_image)
        self.nunchuk = self.app.nunchuk
        self.control = self.app.control
        self.monitorInput=True
        Clock.schedule_interval(self.checkStop, 0.01)
        threading.Thread(target=self.calibrate).start()
    
    def on_dismiss(self, *args):
        
        self.monitorInput=False
        self.object_image.clear_widgets()
        Clock.unschedule(self.checkStop)
        
    def calibrate(self):
        while(self.monitorInput==True):
            self.nunchuk.read()
            if(self.nunchuk.x==0 and self.nunchuk.y==0):
                time.sleep(0.1)
            else:
                self.control.moveStep(self.nunchuk.x, self.nunchuk.y)
                
        
    def checkStop(self, *args):
        if self.nunchuk.module_available==True:
            self.nunchuk.read()
            if self.nunchuk.z == 1:
                self.monitorInput = False
        else:
            self.monitorInput = False

        if (self.monitorInput==False):
            pos = self.calculator.PositionHorizontal(self.rek, self.dek)
            self.app.dobAlt = pos[1]
            self.app.dobAz = pos[0]
            self.app.rektaszension = self.rek
            self.app.deklination = self.dek
            self.dismiss()
            self.screenmanager.current = 'screenControl'
            
            
class ScreenStar(Screen):
    
    calculator = ObjectProperty(KoordCalculation)
    app = ObjectProperty()

    stars = None
    def __init__(self, **kw):
        Screen.__init__(self, **kw)  
        self.stacker.bind(minimum_height=self.stacker.setter('height'))

    def on_enter(self, *args):
        self.stars = CatalogObject.getCalibrationStars()
        star_names = self.stars.keys()
        star_names.sort()
        for n in star_names:
            star = self.stars.get(n)
            pos = self.calculator.PositionHorizontal(float(star[0]), float(star[1]))
            if (pos[1]>20):
                btn = Button(text=n, size_hint_y=None)
                btn.bind(on_press=self.CalibrateOnStar)
                self.stacker.add_widget(btn)
        
    def on_leave(self, *args):
        self.stacker.clear_widgets()
        
    def CalibrateOnStar(self, obj):
        star_name = obj.text
        star = self.stars.get(star_name)
        p = PopupCalibrateStar()
        p.calibrationText = 'Please center on ' + star_name + '\nand press "Z" on controller \nDeklination: ' + self.app.calculator.formatHours(star[0]) + '\nRektaszension: ' + self.app.calculator.formatDegrees(star[1])
        p.calibrationTitle = 'Calibrating on ' + star_name
        p.rek = float(star[0])
        p.dek = float(star[1])
        p.object_id = star[2]
        
        p.open()
