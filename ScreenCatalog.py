# -*- coding: utf-8 -*-
'''
Created on 29.01.2015

@author: martin
'''

from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.togglebutton import ToggleButton

from kivy.graphics import *
from kivy.properties import ObjectProperty, BooleanProperty

from CatalogObject import CatalogObject
from CatalogObject import Constellation
from functools import partial
from KoordCalculation import KoordCalculation

from PopupObjectInfo import PopupObjectInfo

MAIN_MENU = 0
OBJECT_MENU = 1

class MarkButton(Button):
    pass
class ScreenCatalog(Screen):

    element_count = 0
    first_element = 0
    object_type = ''
    PAGE_ELEMENTS = 9
    objects = []
    calculator = ObjectProperty(KoordCalculation)
    visibleOnly = False;
    catalog_items = ObjectProperty()
    nextButtonStatusDisabled = BooleanProperty()
    prevButtonStatusDisabled = BooleanProperty()

    def __init__(self, **kwargs):
        super(ScreenCatalog, self).__init__(**kwargs)

        self.buttonOptVisibleOnly = ToggleButton(text='Option:\nVisible only', on_release=self.optVisibleOnly)
        self.buttonPrevious = Button(text="Previous", id="btnPrevious", on_release=self.previousObjects)
        self.buttonNext = Button(text="Next", id="btnNext", on_release=self.nextObjects)
        self.buttonCategories = Button(text="Categories", id="btnCategories", on_release=self.initCategory)
        self.buttonBack = Button(text="Back", id="btnBack", on_release=self.app.screenControl)
        self.buttonDummy1 = Button(text="", id="btnDummy1")
        self.buttonDummy2 = Button(text="", id="btnDummy2")

        self.bind(nextButtonStatusDisabled=self.buttonNext.setter("disabled"))
        self.bind(prevButtonStatusDisabled=self.buttonPrevious.setter("disabled"))
        
    def on_enter(self, *args):
        Screen.on_enter(self, *args)
        self.initCategory()
        
    
    def on_leave(self, *args):
        Screen.on_leave(self, *args)
        self.catalog_items.clear_widgets()

    def initMenu(self, menu_type):

        self.menubar.clear_widgets()
        if(menu_type==MAIN_MENU):
            self.menubar.add_widget(self.buttonOptVisibleOnly)
            self.menubar.add_widget(self.buttonDummy1)
            self.menubar.add_widget(self.buttonDummy2)
            self.menubar.add_widget(self.buttonBack)
        elif(menu_type==OBJECT_MENU):
            self.menubar.add_widget(self.buttonPrevious)
            self.menubar.add_widget(self.buttonNext)
            self.menubar.add_widget(self.buttonCategories)
            self.menubar.add_widget(self.buttonBack)
                            
    def initCategory(self, *args, **kwargs):

        self.initMenu(MAIN_MENU)
        
        self.catalog_items.clear_widgets()
        if CatalogObject.hasPlanets==True:
            btn = Button(text='Solar System')
            btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_PLANET))
            self.catalog_items.add_widget(btn)
        btn = Button(text='Stars')
        btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_STAR))
        self.catalog_items.add_widget(btn)
        btn = Button(text='Messier')
        btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_MESSIER))
        self.catalog_items.add_widget(btn)
        btn = Button(text='Cluster')
        btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_CLUSTER))
        self.catalog_items.add_widget(btn)
        btn = Button(text='Nebula')
        btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_NEBULA))
        self.catalog_items.add_widget(btn)
        btn = Button(text='Galaxy')
        btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_GALAXY))
        self.catalog_items.add_widget(btn)
        btn = Button(text='Constellation')
        btn.bind(on_press=partial(self.initConstellations, CatalogObject.TYPE_CONSTELLATION))
        self.catalog_items.add_widget(btn)
        
        self.nextButtonStatusDisabled = True
        self.prevButtonStatusDisabled = True
        #self.buttonNext.disabled=True
        #self.buttonPrevious.disabled=True

        self.objects=[]
        self.element_count = 0
        self.first_element = 0
        self.object_type = ''
        
    def optVisibleOnly(self, *args, **kwargs):
        if(args[0].state=="down"):
            self.visibleOnly = True
        else:
            self.visibleOnly = False
        
    def initObjects(self, *args, **kwargs):

        self.initMenu(OBJECT_MENU)

        self.object_type = args[0]
        self.objects = []
        obs = CatalogObject.getObjects(self.object_type, args[1])
        if(self.visibleOnly):
            for o in obs:
                if self.calculator.isVisible(o):
                    self.objects.append(o)
        else:
            self.objects = obs
        self.first_element = 0
        self.element_count = len(self.objects)
        self.listObjects()
        self.prevButtonStatusDisabled = True

    def initConstellations(self, *args, **kwargs):

        self.initMenu(OBJECT_MENU)

        self.object_type = args[0]
        self.objects = Constellation.getConstellations()
        self.first_element = 0
        self.element_count = len(self.objects)
        self.listObjects()
        self.prevButtonStatusDisabled=True

    def nextObjects(self, *args, **kwargs):
        nextFirstElement = self.first_element + ScreenCatalog.PAGE_ELEMENTS
        
        while(1):
            if nextFirstElement>self.element_count:
                self.nextButtonStatusDisabled = True
                break
            
            self.first_element = nextFirstElement
            self.listObjects()
            break
        
    def previousObjects(self, *args, **kwargs):
        nextFirstElement = self.first_element - ScreenCatalog.PAGE_ELEMENTS
        while(1):
            if nextFirstElement<0:
                self.prevButtonStatusDisabled = True
                break
            
            self.first_element = nextFirstElement
            self.listObjects()
            break

    def listObjects(self):
        print "elements: " + str(self.element_count) + ", first: " + str(self.first_element)
        if(self.element_count-self.first_element)<=9:
            print "disable next"
            self.nextButtonStatusDisabled = True
        else:
            print "enable next"
            self.nextButtonStatusDisabled = False

        if(self.first_element)==0:
            print "disable prev"
            self.prevButtonStatusDisabled = True
        else:
            print "enable prev"
            self.prevButtonStatusDisabled = False

        self.catalog_items.clear_widgets()
        
        elements = 0
        for i in range(self.first_element, min((self.first_element+ScreenCatalog.PAGE_ELEMENTS),self.element_count)):
            if self.object_type==CatalogObject.TYPE_MESSIER:
                object_text = self.objects[i].param1 + "\n" + self.objects[i].name
            elif self.object_type==CatalogObject.TYPE_STAR or self.objects[i].object_type==CatalogObject.TYPE_STAR:
                object_text = self.objects[i].name + "\n" + self.objects[i].object_id
            elif self.object_type==CatalogObject.TYPE_CONSTELLATION:
                object_text = self.objects[i].constellation_id + "\n" + self.objects[i].name_ger
            elif self.object_type==CatalogObject.TYPE_PLANET:
                object_text = self.objects[i].name
            else:
                object_text = self.objects[i].object_id 
                if(self.objects[i].object_type!=CatalogObject.TYPE_STAR) and (self.objects[i].param1!=""):
                    object_text += " (" + self.objects[i].param1 + ")" 
                if self.objects[i].name!="": object_text+= "\n" + self.objects[i].name
                if self.objects[i].short_info!="": object_text+= "\n" + self.objects[i].short_info
                
            tw = self.catalog_items.width / self.catalog_items.cols - 4

            if self.object_type==CatalogObject.TYPE_CONSTELLATION:
                btn = Button(text=object_text, halign='center', text_size=(tw,None))
                btn.bind(on_press=partial(self.initObjects, CatalogObject.TYPE_CONSTELLATION_OBJECTS, self.objects[i].constellation_id))

            else:
                if(self.calculator.isVisible(self.objects[i])):
                    btn = MarkButton(text=object_text, halign='center', text_size=(tw,None))
                else:
                    btn = Button(text=object_text, halign='center', text_size=(tw,None))
                
                btn.bind(on_press=partial(self.showObject, self.objects[i]))
            self.catalog_items.add_widget(btn)
            elements +=1

        for e in range(elements,9):
            lbl = Label(text='')
            self.catalog_items.add_widget(lbl)
        
    def showObject(self, *args, **kwargs):
        p = PopupObjectInfo()
        p.ShowCatalogObject(args[0],self.after_goto)

    def after_goto(self):
        self.manager.current='screenControl'
