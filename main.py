'''
Created on 29.01.2015

@author: martin
'''

import kivy

kivy.require('1.10.0')

import os,sys, time
from GitRepositoryUtils import internetAvailable, newVersionAvailable, updateVersion

from KoordCalculation import KoordCalculation

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager
from kivy.properties import ListProperty, NumericProperty, ObjectProperty, StringProperty, BooleanProperty
from kivy.config import ConfigParser, Config
from kivy.core.window import Window
from kivy.uix.image import Image

from ScreenMain import ScreenMain
from ScreenControl import ScreenControl
from ScreenLevel import ScreenLevel
from ScreenStar import ScreenStar
from ScreenCompass import ScreenCompass
from ScreenCatalog import ScreenCatalog
from ScreenStepCalibration import ScreenStepCalibration
from ScreenSettings import ScreenSettings
from ScreenStatus import ScreenStatus
from ScreenStepSpeed import ScreenStepSpeed
from ScreenKoordGoto import ScreenKoordGoto
from ScreenEnd import ScreenEnd
from PopupMessage import PopupMessageYesNo, PopupMessage

from Nunchuk import Nunchuk, Nunchuk_dummy
from Control import Control, Control_dummy
from OAZ import OAZ, OAZ_dummy
from bmp180 import BMP180, BMP180_dummy
from adxl345 import ADXL345, ADXL345_dummy
from hcm5883l import HCM5883L, HCM5883L_dummy
from at24c32 import AT24C32, AT24C32_dummy

from CatalogObject import CatalogObject
from CatalogObject import Constellation

import drivers.stepperDriver as stepdrv

import board
import threading

#Config.set('kivy','keyboard_mode', 'systemandmulti')
Config.set('kivy','keyboard_mode', 'system')
Config.set('graphics','height', '320')
Config.set('graphics','width', '480')
Config.set('graphics','resizable', '0')
Config.set('graphics','fullscreen', '1')
Config.set('graphics','screensaver', '0')
Config.set('graphics','window_state', 'maximized')
Config.set('graphics','multisamples', '0')
Config.set('graphics','show_cursor', '0')
Config.set('input','dobTouch', 'hidinput,/dev/input/by-path/platform-3f204000.spi-event,invert_x=1,invert_y=0')
Config.set('input','mouse', '')
Config.set('input','%(name)s', '')

#sys.stdout = open('_log.txt', 'w')

root_widget = Builder.load_file('DobControl.kv')
board.init()
class DobControlApp(App):
    
    #os.system("sudo hwclock -s")
    
    os.system("sudo pkill fbcp")
    os.system("fbcp &")
    
    config = ConfigParser()
    config.read('dobcontrol.ini')

    
    dobAlt = NumericProperty(-1.0)
    dobAz = NumericProperty(-1.0)
    
    rektaszension = NumericProperty(-1.0)
    deklination = NumericProperty(-1.0)
    
    posLongitude = NumericProperty(0.0)
    posLatitude = NumericProperty(0.0)
    
    previousScreen = None

    #calculator = KoordCalculation()

    nightMode = BooleanProperty(False)
    
    try:
        nunchuk = Nunchuk()
    except:
        nunchuk = Nunchuk_dummy()
    
    #try:
    control = Control()
    #except:
    #    control = Control_dummy()

    try:
        oaz = OAZ()
    except:
        oaz = OAZ_dummy()

    try:
        bmp180 = BMP180()
    except:
        bmp180 = BMP180_dummy()

    try:
        adxl345 = ADXL345()
    except:
        adxl345 = ADXL345_dummy()

    try:
        compass = HCM5883L()
    except:
        compass = HCM5883L_dummy()
        
    try:
        at24c32 = AT24C32()
    except:
        at24c32 = AT24C32_dummy()

    #night vision

    l_color = ListProperty([1,1,1,1])
    l_mark_color = ListProperty([.6,.6,.6,1]) 
    l_calib_color = ListProperty([1,1,1,1]) 
    l_image_color = ListProperty([1,1,1,1])
    l_popup_color = ListProperty([.3,.3,.3,1])
    l_sep_color = ListProperty([47/255., 167/255., 212/255.])
    
    def build_config(self, config):
        config.setdefaults('position',{'position_longitude':0.0, 'position_latitude': 0.0})
    
    def on_config_change(self, config, section, key, value):
        if section=='position':
            if key=='position_longitude':
                self.posLongitude = float(value)
            if key=='position_longitude':
                self.posLatitude = float(value)
            #self.calculator.latitude = self.posLatitude
            #self.calculator.longitude = self.posLongitude
               

    def setTheme(self):
        if(self.nightMode==True):
            self.l_color = [.5,0,0,1]
            self.l_mark_color = [.5,0,0,1]
            self.l_calib_color = [.5,0,0,1]
            self.l_image_color = [.8,0,0,1]
            self.l_sep_color = [.5,0,0,1]
            self.l_popup_color = [0,0,0,1]
            
        else:
            self.l_color = [1,1,1,1]
            self.l_mark_color = [1,1,1,1]
            self.l_calib_color = [1,1,1,1]
            self.l_image_color = [1,1,1,1]
            self.l_sep_color = [47/255., 167/255., 212/255.]
            self.l_popup_color = [.1,.1,.1,1]

    def switchMode(self):

        if (self.nightMode==True):
            self.nightMode=False
        else:
            self.nightMode=True
        self.setTheme()
        
    def build(self):
        
        self.posLongitude = float(self.config.get('position', 'position_longitude'))
        self.posLatitude = float(self.config.get('position', 'position_latitude'))        
        self.calculator = KoordCalculation(self.posLongitude, self.posLatitude)

        sm = ScreenManager()
        sm.add_widget(ScreenMain())
        sm.add_widget(ScreenControl())
        sm.add_widget(ScreenLevel())
        sm.add_widget(ScreenStar())
        sm.add_widget(ScreenCompass())
        sm.add_widget(ScreenCatalog())
        sm.add_widget(ScreenStepCalibration())
        sm.add_widget(ScreenSettings())
        sm.add_widget(ScreenStatus())
        sm.add_widget(ScreenStepSpeed())
        sm.add_widget(ScreenKoordGoto())
        sm.add_widget(ScreenEnd())
        
        return sm

    def on_start(self):

        self.start_image = Image(source="images/start.png", keep_ratio=True)
        self.setTheme()

        Constellation.loadFile()
        CatalogObject.loadFile()
        threading.Thread(target=CatalogObject.loadOnlineObjects).start()

    def checkForUpdate(self):    
        if(internetAvailable()==True):
            if(newVersionAvailable()==True):
                p = PopupMessageYesNo()
                p.showText("New DobControl version is available.\nStart update now?\n\nAfter the update the Raspberry Pi will be restartet.",self.updateVersion)
            else:
                p = PopupMessage()
                p.showText("DobControl version ist up to date.")
    
    def updateVersion(self, *args):
        if (updateVersion()==True):
            os.system("sudo reboot")
        
    def on_stop(self):
        os.system("sudo pkill fbcp &")

    def is_calibrated(self):
        if(self.dobAz<0) or (self.dobAlt<0):
            return False
        else:
            return True

    def screenMain(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenMain";
    def screenCatalog(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenCatalog";
    def screenStar(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenStar";
    def screenStepSpeed(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenStepSpeed";
    def screenStatus(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenStatus";
    def screenSettings(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenSettings";
    def screenStepCalibration(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenStepCalibration";
    def screenControl(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenControl";
    def screenKoordGoto(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenKoordGoto";
    def screenEnd(self, *args):
        self.previousScreen = self.root.current
        self.root.current="screenEnd";
    def screenPrevious(self, *args):
        self.root.current=self.previousScreen
if __name__ == '__main__':
    DobControlApp().run()
