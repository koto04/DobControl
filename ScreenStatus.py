from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty

import GitRepositoryUtils

class ScreenStatus(Screen):

    nun_x_axis = StringProperty('')
    nun_y_axis = StringProperty('')
    x_res = StringProperty('')
    y_res = StringProperty('')
    
    ip_address = StringProperty('')

    app = ObjectProperty()

    nunchuk = None
    control = None

    def on_enter(self, *args):
        
        self.nunchuk = self.app.nunchuk
        self.control = self.app.control
        
        x, y = self.control.getResolution()
        
        self.x_res = str(x*60*60) + '"'
        self.y_res = str(y*60*60) + '"'

        self.nun_x_axis = str(self.nunchuk.x_init)
        self.nun_y_axis = str(self.nunchuk.y_init)

        if GitRepositoryUtils.internetAvailable() == True:
            self.ip_address = GitRepositoryUtils.getIP()
        
