'''
Created on 30.01.2015

@author: martin
'''
from kivy.properties import ListProperty
from kivy.uix.widget import Widget
from kivy.animation import Animation

from kivy.config import ConfigParser
from at24c32 import AT24C32,AT24C32_dummy
from at24c32 import EEPROM_LEVEL_XOFFSET
from at24c32 import EEPROM_LEVEL_YOFFSET


import math

class CalibrationBall(Widget):
    
    CONST_X_CENTER = 512
    CONST_Y_CENTER = 512

    X_OFFSET = -41
    Y_OFFSET = -137
    CONST_MAX_WAY = 100 
    
    color = ListProperty([1, .0, .0, 1])

    config = ConfigParser()
    config.read('dobcontrol.ini')
            
    def __init__(self, **kwargs):
        super(CalibrationBall, self).__init__(**kwargs)
        try:
            self.at24c32 = AT24C32()
        except:
            self.at24c32 = AT24C32_dummy()
        CalibrationBall.X_OFFSET = self.at24c32.readLong(EEPROM_LEVEL_XOFFSET)
        CalibrationBall.Y_OFFSET = self.at24c32.readLong(EEPROM_LEVEL_YOFFSET)

    def move(self, x, y):

        x=float((x*-1)+CalibrationBall.X_OFFSET)
        y=float((y*-1)+CalibrationBall.Y_OFFSET)
        dif_x = (x/CalibrationBall.CONST_X_CENTER) * self.CONST_MAX_WAY*4
        dif_y = (y/CalibrationBall.CONST_Y_CENTER) * self.CONST_MAX_WAY*4

        if(dif_x>self.CONST_MAX_WAY): dif_x = self.CONST_MAX_WAY
        if(dif_x<(-1*self.CONST_MAX_WAY)): dif_x = (self.CONST_MAX_WAY * -1)
        
        if(dif_y>self.CONST_MAX_WAY): dif_y = self.CONST_MAX_WAY
        if(dif_y<(-1*self.CONST_MAX_WAY)): dif_y = (self.CONST_MAX_WAY * -1)
        
        new_x = self.parent.width/2 + dif_x
        new_y = self.parent.height/2 + dif_y + self.parent.pos[1]

        if(math.fabs(dif_x)<4) and (math.fabs(dif_y)<4):
            self.color = [0,.8,0,1]
        else:
            self.color = [1,0,0,1]
        
        anim = Animation(x=new_x-10, y=new_y-10, duration=0.1)
        anim.start(self)
    def savePlane(self, x, y):
        CalibrationBall.X_OFFSET = x
        CalibrationBall.Y_OFFSET = y
        self.at24c32.writeLong(EEPROM_LEVEL_XOFFSET, x)
        self.at24c32.writeLong(EEPROM_LEVEL_YOFFSET, y)
        
