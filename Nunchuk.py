import time
import sys

class Nunchuk():
    module_available = True
    bus = None
    address = None
    data = bytearray()
    xraw = 0
    yraw = 0
    buttons=0
    x = 0
    y = 0
    c = 0
    z = 0
    x_steps = []
    y_steps = []
    x_init = 0
    y_init = 0
    
    dy=0
    
    def __init__(self, address = 0x52):
        smbus=__import__('smbus')
        self.bus = smbus.SMBus(1)
        self.address = address
        self.bus.write_byte_data(self.address,0xF0,0x55)
        time.sleep(0.1)
        self.bus.write_byte_data(self.address,0xFB,0x00)
        time.sleep(0.1)
        self.bus.write_byte(self.address,0x00)
        self.x_init = self.bus.read_byte(self.address)
        self.y_init = self.bus.read_byte(self.address)
        print "Nunchuk Init: x=" + str(self.x_init) + ", y=" + str(self.y_init)
        x_high = (256-self.x_init) / 3
        x_low = self.x_init / 3
        y_high = (256-self.y_init) / 3
        y_low = self.y_init / 3
        self.x_steps = [0,1,x_low, x_low*2, self.x_init-2, self.x_init+2, self.x_init+x_high, self.x_init+x_high*2, 255, 256]
        self.y_steps = [0,1,y_low, y_low*2, self.y_init-2, self.y_init+2, self.y_init+y_high, self.y_init+y_high*2, 255, 256]
        print(self.x_steps)
        print(self.y_steps)

    def read(self):

        exc=0
        
        while (True):
            try:
                self.bus.write_byte(self.address,0x00)
                self.xraw = self.bus.read_byte(self.address)
                self.yraw = self.bus.read_byte(self.address)
                self.bus.write_byte(self.address,0x05)
                self.buttons = self.bus.read_byte(self.address)
                break
            except:
                print "Nunchuk Exception"
                time.sleep(.2)
                exc+=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                self.x = 0
                self.y = 0
                self.c = 0
                self.z = 0
                return

        for i in range(0,len(self.x_steps)):
            if(self.xraw>=self.x_steps[i]) and (self.xraw<self.x_steps[i+1]):
                self.x = i-4
                break

        for i in range(0,len(self.y_steps)):
            if(self.yraw>=self.y_steps[i]) and (self.yraw<self.y_steps[i+1]):
                self.y = i-4
                break

        self.z = ~self.buttons & 0x01
        self.c = (~self.buttons>>1) & 0x01
        

class Nunchuk_dummy():
    
    module_available = False
    x = 0
    y = 0
    c = 0
    z = 0
    x_steps = []
    y_steps = []
    x_init = 0
    y_init = 0
    kb = None    
        
    
    def __init__(self):
        if sys.platform[:3] == 'win':
            self.kb=__import__('msvcrt')
        elif sys.platform[:3] == 'lin':
            self.kb= sys.stdin.fileno()
                    
    def read(self):
      self.x = 0
      self.y = 0
      self.c = 0
      self.z = 0
      ch=""
      
      x = self.kb.kbhit()
      if x: 
        ch = self.kb.getch()
        print ch
      if(ch=='a'):
        self.x=-1
      if(ch=='s'):
        self.x=1
      if(ch=='w'):
        self.y=1
      if(ch=='y'):
        self.y=-1
      if(ch=='c'):
        self.c=1
      if(ch=='z'):
        self.z=1
"""
n=Nunchuk()
while(True):
    n.read()
    print "x: " + str(n.x) + ", y:" + str(n.y) + ", c:" + str(n.c) + ", z:" + str(n.z)
    time.sleep(0.2)
"""        
