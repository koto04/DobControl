from kivy.uix.screenmanager import Screen
from kivy.uix.image import Image
from kivy.clock import Clock
from functools import partial
from kivy.animation import Animation
from kivy.properties import ObjectProperty, NumericProperty, StringProperty

import time
class CompassImage(Image):
    angle = NumericProperty(0)

    def update(self, angle):
        self.angle = angle
        #Animation.cancel_all(self)
        #Animation(center=self.center, angle=angle).start(self)

class ScreenCompass(Screen):
    
    app = ObjectProperty()
    message = StringProperty('')
    
    compass = None
    compassImage = ObjectProperty(CompassImage)
    def on_enter(self, *args):
        Screen.on_enter(self, *args)
        self.compass = self.app.compass
        Clock.schedule_interval(self.update, 0.5)
    
    def on_pre_leave(self, *args):
        Screen.on_pre_leave(self, *args)
        Clock.unschedule(self.update)
    def update(self, dt):
        self.compassImage.update(self.compass.getHeading())
       
    def calibarte(self, *args):
        self.message = "Calibrating the Magnetometer ........ Gain"
        time.sleep(.1)
        self.compass.calibrate(1)
        self.message = "Please rotate the magnethometer with in 15 minutes..."
        time.sleep(.1)
        self.compass.calibrate(2)
        
