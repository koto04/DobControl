# -*- coding: utf-8 -*-

import time
import math

from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.clock import Clock
from functools import partial
from at24c32 import EEPROM_X_STEPS, EEPROM_Y_STEPS

class ScreenStepCalibration(Screen):

    nunchuk =  None
    control = None
    eeprom = None

    app = ObjectProperty()

    x_steps = StringProperty('0')
    y_steps = StringProperty('0')

    x_current = 0
    y_current = 0

    x_last = StringProperty('0')
    y_last = StringProperty('0')

    clockEvent = ObjectProperty()

    moving=False
    
    def on_enter(self, *args):
        Screen.on_enter(self, *args)

        self.nunchuk = self.app.nunchuk
        self.control = self.app.control
        self.eeprom = self.app.at24c32

        self.clockEvent = Clock.schedule_interval(self.checkMove, 0.01)
        x = self.eeprom.readLong(EEPROM_X_STEPS)
        y = self.eeprom.readLong(EEPROM_Y_STEPS)
        self.x_last = str(x*10)
        self.y_last = str(y*10)

    def on_leave(self, *args):
        Screen.on_pre_leave(self, *args)
        
        if(self.clockEvent):
            Clock.unschedule(self.checkMove)


    def checkMove(self, dt):
        if self.moving==True:
            return
        
        self.nunchuk.read()

        while (self.nunchuk.x!=0 or self.nunchuk.y!=0):
            self.moving = True
            move = self.control.moveRaw(self.nunchuk.x, self.nunchuk.y)
            self.x_current += move["dx"]
            self.y_current += move["dy"]
                    
            self.x_steps = str(math.fabs(self.x_current))
            self.y_steps = str(math.fabs(self.y_current))
            mx = math.fabs(self.nunchuk.x)
            my = math.fabs(self.nunchuk.y)
            if(mx==1) or (my==1):
                time.sleep(0.1)

            self.nunchuk.read()

        self.moving = False        

    def saveValues(self):

        x=long(math.fabs(self.x_current/10))
        y=long(math.fabs(self.y_current/10))

        self.eeprom.writeLong(EEPROM_X_STEPS, x)
        self.eeprom.writeLong(EEPROM_Y_STEPS, y)

        control.setSteps()

    def clearValues(self):

        self.x_current = 0
        self.y_current = 0
        self.x_steps = str(math.fabs(self.x_current))
        self.y_steps = str(math.fabs(self.y_current))
