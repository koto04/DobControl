
import struct
import sys
from time import sleep
from PopupMessage import PopupMessage


# BMP180 constants

AC1_REG = 0xAA
AC2_REG = 0xAC
AC3_REG = 0xAE
AC4_REG = 0xB0
AC5_REG = 0xB2
AC6_REG = 0xB4
B1_REG = 0xB6
B2_REG = 0xB8
MB_REG = 0xBA
MC_REG = 0xBC
MD_REG = 0xBE

CMD_REG = 0xF4
DATA_REG = 0xF6

CMD_TEMP = 0x2E
CMD_PRESSURE = 0x34

class BMP180:
    module_available = True
    bus = None
    address = None
    ac1 = 0
    ac2 = 0
    ac3 = 0
    ac4 = 0
    ac5 = 0
    ac6 = 0
    b1 = 0
    b2 = 0
    mb = 0
    mc = 0
    md = 0
    online = False;
    
    def __init__(self, address = 0x77):        
        smbus=__import__('smbus')
        self.bus = smbus.SMBus(1)
        self.address = address
        self.bus.write_byte(self.address,AC1_REG)
        data=[]
        for i in range(22):
            data.append(self.bus.read_byte(self.address))
        
        #data = self.bus.read_i2c_block_data(self.address, AC1_REG, 22)
        tmp = ""
        for i in range(0,22):
            tmp += chr(data[i])
        s = struct.unpack(">hhhHHHhhhhh",tmp)
        self.ac1 = s[0]
        self.ac2 = s[1]
        self.ac3 = s[2]
        self.ac4 = s[3]
        self.ac5 = s[4]
        self.ac6 = s[5]
        self.b1 = s[6]
        self.b2 = s[7]
        self.mb = s[8]
        self.mc = s[9]
        self.md = s[10]
        self.online=True
    def getData(self):
        if(self.online==False):
            return {"temperature": 0, "pressure": 0}

        exc=0        
        while(True):
            data=[]
            try:
                self.bus.write_byte_data(self.address, CMD_REG, CMD_TEMP)
                sleep(0.005)
                self.bus.write_byte(self.address,DATA_REG)
                data.append(self.bus.read_byte(self.address))
                data.append(self.bus.read_byte(self.address))
                self.bus.write_byte_data(self.address, CMD_REG, CMD_PRESSURE)
                sleep(0.005)
                self.bus.write_byte(self.address,DATA_REG)
                data.append(self.bus.read_byte(self.address))
                data.append(self.bus.read_byte(self.address))
                break
            except:
                sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                return {"temperature": 0, "pressure": 0}
            
        tmp = chr(data[0]) + chr(data[1])
        pre = chr(data[2]) + chr(data[3])
        ut = struct.unpack(">H",tmp)[0]
        up = struct.unpack(">H",pre)[0]
        x1 = (ut-self.ac6)*self.ac5/0x008000
        x2 = self.mc*0x000800 / (x1+self.md)
        b5 = x1+x2
        t = (b5+8) / 0x0010 * 0.1
        b6 = b5 - 4000
        x1=(self.b2*(b6*b6/0x001000))/0x000800
        x2=self.ac2*b6/0x000800
        x3=x1+x2
        b3=((self.ac1*4+x3)+2)/4
        x1=self.ac3*b6/0x002000
        x2=(self.b1*(b6*b6/0x001000))/0x010000
        x3=((x1+x2)+2)/0x000004
        b4 = self.ac4 * (x3+32768)/0x008000
        b7 = (up-b3) * 50000
        if(b7<0x080000000):
            p=(b7*2)/b4
        else:
            p=(b7/b4)*2
        x1=(p/0x000100) * (p/0x000100)
        x1 = (x1*3038)/0x010000
        x2=(-7357*p)/0x010000
        p=p+(x1+x2+3791)/0x000010
        p=p*0.01
        return {"temperature": t, "pressure": p}
        
class BMP180_dummy:
    module_available = False
    def __init__(self):        
        pass
    def getData(self):
        return {"temperature": 20, "pressure": 1000}
        
