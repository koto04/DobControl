import drivers.spiCom as spicom

import board
import time

DRIVER_TYPE_BIGEASYDRIVER = 0x00
DRIVER_TYPE_RAPS128 = 0x01
DRIVER_TYPE_TMC2130 = 0x02
DRIVER_TYPE_NONE = 0xFF

DRIVER_AZ = 0x01
DRIVER_ALT = 0x02
DRIVER_BOTH = 0x03

DRIVER_AZ_DATA_OFFSET = 0
DRIVER_ALT_DATA_OFFSET = 4

#port B Configuration
DRIVER_CFG_USTEP_M1 = 0x01
DRIVER_CFG_USTEP_M2 = 0x02
DRIVER_CFG_USTEP_M3 = 0x04
DRIVER_CFG_EN = 0x08

#port A Steps and Directions
DRIVER_DIR  = 0x01

DIRECTION_NEGATIV = 0x00#DRIVER_DIR
DIRECTION_POSITIV = DRIVER_DIR#0x00

DIRECTION_MASK = (DRIVER_DIR<<DRIVER_AZ_DATA_OFFSET) | (DRIVER_DIR<<DRIVER_ALT_DATA_OFFSET)
#STEP_MASK = (DRIVER_DIR<<DRIVER_AZ_DATA_OFFSET) | (DRIVER_DIR<<DRIVER_ALT_DATA_OFFSET)

USTEP_CONFIG_MASK = DRIVER_CFG_USTEP_M1<<DRIVER_AZ_DATA_OFFSET | DRIVER_CFG_USTEP_M2<<DRIVER_AZ_DATA_OFFSET | DRIVER_CFG_USTEP_M3<<DRIVER_AZ_DATA_OFFSET | DRIVER_CFG_USTEP_M1<<DRIVER_ALT_DATA_OFFSET | DRIVER_CFG_USTEP_M2<<DRIVER_ALT_DATA_OFFSET | DRIVER_CFG_USTEP_M3<<DRIVER_ALT_DATA_OFFSET

IOCONTROLLER_ADDR = 0x40
IOCONTROLLER_IOCTRL = 0x0A
IOCONTROLLER_DIRA = 0x00
IOCONTROLLER_DIRB = 0x01
DRIVER_STEP_DIR_REG = 0x15 #OLATA
DRIVER_CFG_REG = 0x14 #OLATB

IOCONTROLLER_SPI_WRITE = 0x00
IOCONTROLLER_SPI_READ = 0x01

STEP_DELAY = 0.0009
#STEP_DELAY = 0.001
STEP_PULSE_TIME = 0.0001

class StepperDriver(object):

    ioController = None
    driverConfig = 0
    driverDirection = 0
    lastDirection = -1
    lastDriverConfig = 0xFF

    driverType = DRIVER_TYPE_NONE
    lastStepTime = 0

    @classmethod
    def getDriver(cls, driverType):
        driverClass = None
        print cls.__name__
        if cls.__name__ == "StepperDriver":
            if driverType==DRIVER_TYPE_TMC2130:
                dirverModule=__import__('drivers.tmc2130',fromlist=['TMC2130'])
                driverClass = getattr(dirverModule, 'TMC2130')
            elif driverType==DRIVER_TYPE_BIGEASYDRIVER:
                dirverModule=__import__('drivers.bigEasyDriver',fromlist=['BigEasyDriver'])
                driverClass = getattr(dirverModule, 'TMC2130')
            elif driverType==DRIVER_TYPE_RAPS128:
                dirverModule=__import__('drivers.raps128',fromlist=['RAPS128'])
                driverClass = getattr(dirverModule, 'RAPS128')

        return driverClass()
    
    def __init__(self):
        self.ioController = spicom.SPI1Device(board.GPIO_SPI1_CE2, spicom.MODE_CPOL0_CPHA0)
        
        self.initIOController()
        
    def __delete_(self):
        self.ioController.close()
    
    def initIOController(self):
        self.ioController.txrx([IOCONTROLLER_ADDR, IOCONTROLLER_DIRA, 0x00])
        self.ioController.txrx([IOCONTROLLER_ADDR, IOCONTROLLER_DIRB, 0x00])

    def initConfiguration(self):
        self.writeUstepConfig(self.getMaxUSteps(), self.getMaxUSteps(), force=True)
        self.writeStep(1,1)
        
    def writeStep(self, azStep=0, altStep=0):
        
        self.driverDirection &= DIRECTION_MASK
        
        if self.lastDirection != self.driverDirection:
            self.ioController.txrx([IOCONTROLLER_ADDR, DRIVER_STEP_DIR_REG, self.driverDirection])
            time.sleep(0.001)
            self.lastDirection = self.driverDirection
            
        while (time.time()-self.lastStepTime)<STEP_DELAY:
            pass
        
        board.GPIO.output(board.GPIO_AZ_STEP, board.GPIO.HIGH if azStep!=0 else board.GPIO.LOW)
        board.GPIO.output(board.GPIO_ALT_STEP, board.GPIO.HIGH if altStep!=0 else board.GPIO.LOW)
        time.sleep(STEP_PULSE_TIME)
        board.GPIO.output(board.GPIO_AZ_STEP, board.GPIO.LOW)
        board.GPIO.output(board.GPIO_ALT_STEP, board.GPIO.LOW)
        self.lastStepTime=time.time()


    #Sets the direction based on pos/neg values vor Az and Alt
    def setDirectionAzAlt(self, az, alt):
        self.driverDirection = 0
        if(az>0):
            self.driverDirection |= (self.driverDirection & (0xF0>>DRIVER_AZ_DATA_OFFSET)) | (DIRECTION_POSITIV<<DRIVER_AZ_DATA_OFFSET)
        else:
            self.driverDirection |= (self.driverDirection & (0xF0>>DRIVER_AZ_DATA_OFFSET)) | (DIRECTION_NEGATIV<<DRIVER_AZ_DATA_OFFSET)
        if(alt>0):
            self.driverDirection |= (self.driverDirection & (0xF0>>DRIVER_ALT_DATA_OFFSET)) | (DIRECTION_POSITIV<<DRIVER_ALT_DATA_OFFSET)
        else:
            self.driverDirection |= (self.driverDirection & (0xF0>>DRIVER_ALT_DATA_OFFSET)) | (DIRECTION_NEGATIV<<DRIVER_ALT_DATA_OFFSET)
    def setDirection(self, direction, driver):
        
        if (driver & DRIVER_AZ):
            self.driverDirection |= (self.driverDirection & (0xF0>>DRIVER_AZ_DATA_OFFSET)) | (direction<<DRIVER_AZ_DATA_OFFSET)
        if (driver & DRIVER_ALT):
            self.driverDirection |= (self.driverDirection & (0xF0>>DRIVER_ALT_DATA_OFFSET)) | (direction<<DRIVER_ALT_DATA_OFFSET)

    #dummy API functions
    def getMaxUSteps(self):
        return 1
    
    def writeUstepConfig(self, azUSteps, altUSteps, force=False):
        pass
    
