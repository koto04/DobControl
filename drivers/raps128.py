
import stepperDriver as stpdrv

MAX_USTEPS = 128

class RAPS128(stpdrv.StepperDriver):
    
    def __init__(self):
        super(RAPS128, self).__init__()
        self.driverType = stpdrv.DRIVER_TYPE_RAPS128
        self.initConfiguration()
    
    def writeUstepConfig(self, azUSteps, altUSteps, force=False):
        
        azConfig = self.calcUStepConfigFromUSteps(azUSteps)
        altConfig = self.calcUStepConfigFromUSteps(altUSteps)

        usteps = (azConfig<<stpdrv.DRIVER_AZ_DATA_OFFSET) | (altConfig<<stpdrv.DRIVER_ALT_DATA_OFFSET)
        
        #requested config currently set?
        if (usteps!=(self.lastDriverConfig)) or (force==True): 
            
            self.ioController.txrx([stpdrv.IOCONTROLLER_ADDR, stpdrv.DRIVER_CFG_REG, usteps])            
            self.lastDriverConfig = usteps
        
    def calcUStepConfigFromUSteps(self, usteps):
        
        uStepConfig = stpdrv.DRIVER_CFG_EN
        
        if(usteps==128):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M1 | stpdrv.DRIVER_CFG_USTEP_M2 | stpdrv.DRIVER_CFG_USTEP_M3
        elif(usteps==64):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M2 | stpdrv.DRIVER_CFG_USTEP_M3
        elif(usteps==32):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M1 | stpdrv.DRIVER_CFG_USTEP_M3
        elif(usteps==16):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M3
        elif(usteps==8):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M2 | stpdrv.DRIVER_CFG_USTEP_M1
        elif(usteps==4):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M2
        elif(usteps==2):
            uStepConfig |= stpdrv.DRIVER_CFG_USTEP_M1
        elif(usteps==1):
            uStepConfig |= 0
        
        return uStepConfig
    
    def getMaxUSteps(self):
        return MAX_USTEPS
