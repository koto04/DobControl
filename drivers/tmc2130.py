import drivers.spiCom as spicom

import stepperDriver as stpdrv
import board
import time

WRITE_FLAG     =(1<<7) #write flag
READ_FLAG      =(0<<7) #read flag
REG_GCONF      =0x00
REG_GSTAT      =0x01
REG_IOIN       =0x04
REG_IHOLD_IRUN =0x10
REG_TPOWERDOWN =0x11
REG_CHOPCONF   =0x6C
REG_COOLCONF   =0x6D
REG_DCCTRL     =0x6E
REG_DRVSTATUS  =0x6F
REG_XDIRECT    =0x2D

CHOPCONF_DEFAULT = 0x10008008
#CHOPCONF_DEFAULT =  0x000100C5
#CHOPCONF_DEFAULT = 0x000100C3

MAX_USTEPS = 256

IHOLD = 0x04#base 32
IRUN = 0x1F
IHOLDDELAY = 0x06 #Delay per current reduction step in multiple of 2^18 clocks
TPOWERDOWN = 0x10

class TMC2130(stpdrv.StepperDriver):
    
    spiAzDriver = None
    spiAltDriver = None
    
    def __init__(self):
        super(TMC2130, self).__init__()
        self.driverType = stpdrv.DRIVER_TYPE_TMC2130
        self.spiAzDriver = spicom.SPI1Device(board.GPIO_SPI1_CE0, spicom.MODE_CPOL1_CPHA1)
        self.spiAltDriver = spicom.SPI1Device(board.GPIO_SPI1_CE1, spicom.MODE_CPOL1_CPHA1)
        time.sleep(0.1)
        print("init start")
        self.initDriver(self.spiAzDriver)
        self.initDriver(self.spiAltDriver)
        print("init ende")
        self.ioController.txrx([stpdrv.IOCONTROLLER_ADDR, stpdrv.DRIVER_CFG_REG, 0x00])
   
        self.initConfiguration()
        
    def initDriver(self, driver):
        x = self.readRegister(driver, REG_GSTAT)
        time.sleep(0.01)
        ret = self.writeRegister(driver, REG_GCONF, 0x00000001) #voltage on AIN is current reference
        time.sleep(0.01)
        ret = self.writeRegister(driver, REG_CHOPCONF, CHOPCONF_DEFAULT) 
        time.sleep(0.01)
        ret = self.writeRegister(driver, REG_IHOLD_IRUN, 0x00000000 | (IHOLD + (IRUN<<8) + (IHOLDDELAY<<16))) 
        time.sleep(0.01)
        ret = self.writeRegister(driver, REG_TPOWERDOWN, TPOWERDOWN) 
        time.sleep(0.01)

  
    def writeRegister(self, driver, reg, value):
        data = []
        data.append(WRITE_FLAG|reg)
        data.append((value>>24)&0xFF)
        data.append((value>>16)&0xFF)
        data.append((value>>8)&0xFF)
        data.append((value>>0)&0xFF)
        ret = driver.txrx(data)
        #print ''.join(format(x, '02x') for x in ret)
        return ret

    def readRegister(self, driver, reg):
        data = []
        data.append(reg)
        data.append(0x00)
        data.append(0x00)
        data.append(0x00)
        data.append(0x00)
        
        driver.txrx(data)
        ret=driver.txrx(data)
        #print ''.join(format(x, '02x') for x in ret)
        return ret
  
    def writeUstepConfig(self, azUSteps, altUSteps, force=False):
        
        azConfig = self.calcUStepConfigFromUSteps(azUSteps)
        altConfig = self.calcUStepConfigFromUSteps(altUSteps)
        
        usteps = (azConfig<<stpdrv.DRIVER_AZ_DATA_OFFSET) | (altConfig<<stpdrv.DRIVER_ALT_DATA_OFFSET)
        #requested config differ from currently set or force set?
        if (usteps==(self.lastDriverConfig)) and (force==False): 
            return
                
        #update AZ driver?
        if (azConfig != (( self.lastDriverConfig>>stpdrv.DRIVER_AZ_DATA_OFFSET) & 0x0F)):
            reg_value = CHOPCONF_DEFAULT | (azConfig<<24)
            #print "write Az"
            self.writeRegister(self.spiAzDriver, REG_CHOPCONF, reg_value) 
            #print "read Az"
            #self.readRegister(self.spiAzDriver, REG_CHOPCONF)
        #update AZ driver?
        if (altConfig != (( self.lastDriverConfig>>stpdrv.DRIVER_ALT_DATA_OFFSET) & 0x0F)):
            reg_value = CHOPCONF_DEFAULT | (altConfig<<24)
            #print "write Alt"
            self.writeRegister(self.spiAltDriver, REG_CHOPCONF, reg_value) 
            #print "read Alt"
            #self.readRegister(self.spiAltDriver, REG_CHOPCONF)
        
        time.sleep(0.001)
            
        self.lastDriverConfig = usteps
        
    def calcUStepConfigFromUSteps(self, usteps):
        
        ret = 0b0000
        if(usteps==128):
            ret = 0b0001
        elif(usteps==64):
            ret = 0b0010
        elif(usteps==32):
            ret = 0b0011
        elif(usteps==16):
            ret = 0b0100
        elif(usteps==8):
            ret = 0b0101
        elif(usteps==4):
            ret = 0b0110
        elif(usteps==2):
            ret = 0b0111
        elif(usteps==1):
            ret = 0b1000
        return ret

    def getMaxUSteps(self):
        return MAX_USTEPS
