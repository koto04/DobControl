
import board
import time

MODE_CPOL0_CPHA0 = 0
MODE_CPOL0_CPHA1 = 1
MODE_CPOL1_CPHA0 = 2
MODE_CPOL1_CPHA1 = 3

CYCLE_DELAY = 0#0.000001

class SPI1Device(object):
    
    spiInitialized = False
    cs = None
    mode = MODE_CPOL0_CPHA0
    clockValid = 0 #data valid
    clockInvalid = 0 #data invalid
    
    @classmethod
    def initSPI(cls):
        
        SPI1Device.spiInitialized = True
        
    def __init__(self, cs, mode=MODE_CPOL0_CPHA0):
        if SPI1Device.spiInitialized==False:
            SPI1Device.initSPI()
        self.cs = cs
        self.setMode(mode)

    def setMode(self, mode):
        self.mode = mode
        if self.mode == MODE_CPOL0_CPHA0 or self.mode == MODE_CPOL1_CPHA1:
            self.clockValid = board.GPIO.HIGH
            self.clockInvalid = board.GPIO.LOW
        else:
            self.clockValid = board.GPIO.LOW
            self.clockInvalid = board.GPIO.HIGH

    def txrx(self, tx_buffer):
        if self.mode == MODE_CPOL0_CPHA1 or self.mode == MODE_CPOL1_CPHA1:
            board.GPIO.output(board.GPIO_SPI1_CLK, self.clockValid)
        else:
            board.GPIO.output(board.GPIO_SPI1_CLK, self.clockInvalid)

        board.GPIO.output(self.cs, board.GPIO.LOW)
        time.sleep(CYCLE_DELAY)
        bits = len(tx_buffer) * 8
        rx_buffer = bytearray(len(tx_buffer))
        if self.mode == MODE_CPOL0_CPHA1 or self.mode == MODE_CPOL1_CPHA1:
            board.GPIO.output(board.GPIO_SPI1_CLK, self.clockInvalid)
            
        for bytePosition in range(0,len(tx_buffer)):
            for bitPosition in range(0,8):
                rx_bit = 0
                if(tx_buffer[bytePosition]&(0x80>>bitPosition))==0:
                    #print "1"
                    board.GPIO.output(board.GPIO_SPI1_MOSI, board.GPIO.LOW)
                else:
                    #print "0"
                    board.GPIO.output(board.GPIO_SPI1_MOSI, board.GPIO.HIGH)
                bits-=1
                rx_bit = self.clockCycle(bits==0)
                if (rx_bit) : rx_buffer[bytePosition]|=(0x80>>bitPosition)      
        
        time.sleep(CYCLE_DELAY)
        board.GPIO.output(self.cs, board.GPIO.HIGH)
        return rx_buffer        
                    
    def clockCycle(self, last=False):
        time.sleep(CYCLE_DELAY)
        board.GPIO.output(board.GPIO_SPI1_CLK, self.clockValid)
        ret = board.GPIO.input(board.GPIO_SPI1_MISO)
        time.sleep(CYCLE_DELAY)
        if last==True and (self.mode == MODE_CPOL0_CPHA1 or self.mode != MODE_CPOL1_CPHA1):
            pass
        else:
            board.GPIO.output(board.GPIO_SPI1_CLK, self.clockInvalid)
        return ret
        
