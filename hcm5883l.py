import math
import sys
import time
from at24c32 import AT24C32
from at24c32 import EEPROM_X_GAIN_ERROR, EEPROM_Y_GAIN_ERROR, EEPROM_Z_GAIN_ERROR
from at24c32 import EEPROM_X_OFFSET, EEPROM_Y_OFFSET, EEPROM_Z_OFFSET
from PopupMessage import PopupMessage

#HCM5883L constants

CONFIG_REGISTER_A = 0x00
CONFIG_REGISTER_B = 0x01
MODE_REGISTER = 0x02
DATA_OUTPUT_REGISTER = 0x03
DATA_OUTPUT_X_REGISTER = 0x03
DATA_OUTPUT_Y_REGISTER = 0x07
DATA_OUTPUT_Z_REGISTER = 0x05

OUTPUT_RATE_0_75HZ = 0x00
OUTPUT_RATE_1_50HZ = 0x04
OUTPUT_RATE_3HZ = 0x08
OUTPUT_RATE_7_50HZ = 0x0C
OUTPUT_RATE_15HZ = 0x10  #default
OUTPUT_RATE_30HZ = 0x14
OUTPUT_RATE_75HZ = 0x18

AVARAGE_1 = 0x00 #default
AVARAGE_2 = 0x20
AVARAGE_4 = 0x40
AVARAGE_8 = 0x60

MEASURE_MODE_NORMAL = 0x00 #default
MEASURE_MODE_POSITIVE = 0x01
MEASURE_MODE_NEGATIVE = 0x02

GAIN_0880 = 0x00
GAIN_1300 = 0x20 #default
GAIN_1900 = 0x40
GAIN_2500 = 0x60
GAIN_4000 = 0x80
GAIN_4700 = 0xA0
GAIN_5600 = 0xC0
GAIN_8100 = 0xE0

OPERATION_MODE_CONTINUOUS = 0x00
OPERATION_MODE_SINGLE = 0x01 #default
OPERATION_MODE_IDLE = 0x02

DECLINATION = 0.0404

COMPASS_XY_EXCITATION = 1160
COMPASS_Z_EXCITATION = 1080

class HCM5883L:
    module_available = True
    bus=None
    address = None
    compass_x_offset=0
    compass_y_offset=0
    compass_z_offset=0
    compass_gain_fact=1
    compass_x_scalled=0
    compass_y_scalled=0
    compass_z_scalled=0
    compass_x_gainError=0
    compass_y_gainError=0
    compass_z_gainError=0
    bearing=0
    compass_x=0
    compass_y=0
    compass_z=0
    eeprom = None

    def __init__(self, address = 0x1e):
        smbus=__import__('smbus')
        self.bus = smbus.SMBus(1)
        self.address = address
        self.setOperationMode(OPERATION_MODE_CONTINUOUS)
        self.setGain(GAIN_1300)
        self.eeprom = AT24C32()
        self.compass_x_gainError = self.eeprom.readFloat(EEPROM_X_GAIN_ERROR)
        self.compass_y_gainError = self.eeprom.readFloat(EEPROM_Y_GAIN_ERROR)
        self.compass_z_gainError = self.eeprom.readFloat(EEPROM_Z_GAIN_ERROR)
        self.compass_x_offset = self.eeprom.readFloat(EEPROM_X_OFFSET)
        self.compass_y_offset = self.eeprom.readFloat(EEPROM_Y_OFFSET)
        self.compass_z_offset = self.eeprom.readFloat(EEPROM_Z_OFFSET)
        
    def setRate(self, rate):
        exc=0        
        while(True):
            try:
                value = self.bus.read_byte_data(self.address, CONFIG_REGISTER_A)
                value &= ~0x1C;
                value |= rate;  
                self.bus.write_byte_data(self.address,CONFIG_REGISTER_A, value)
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break

    def setAvarage(self, avarage):
        exc=0        
        while(True):
            try:
                value = self.bus.read_byte_data(self.address, CONFIG_REGISTER_A)
                value &= ~0x60;
                value |= avarage;  
                self.bus.write_byte_data(self.address,CONFIG_REGISTER_A, value)
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break

    def setMeasurementMode(self, mode):
        exc=0        
        while(True):
            try:
                value = self.bus.read_byte_data(self.address, CONFIG_REGISTER_A)
                value &= ~0x03;
                value |= mode;  
                self.bus.write_byte_data(self.address,CONFIG_REGISTER_A, value)
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break

    def setGain(self, gain):
        exc=0        
        while(True):
            try:
                value = self.bus.read_byte_data(self.address, CONFIG_REGISTER_B)
                value &= ~0xE0;
                value |= gain;  
                self.bus.write_byte_data(self.address,CONFIG_REGISTER_B, value)
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break

        if(gain==GAIN_0880): self.compass_gain_fact = 0.73
        if(gain==GAIN_1300): self.compass_gain_fact = 0.92
        if(gain==GAIN_1900): self.compass_gain_fact = 1.22
        if(gain==GAIN_2500): self.compass_gain_fact = 1.52
        if(gain==GAIN_4000): self.compass_gain_fact = 2.27
        if(gain==GAIN_4700): self.compass_gain_fact = 2.56
        if(gain==GAIN_5600): self.compass_gain_fact = 3.03
        if(gain==GAIN_8100): self.compass_gain_fact = 4.35

    def setOperationMode(self, mode):
        exc=0        
        while(True):
            try:
                value = self.bus.read_byte_data(self.address, MODE_REGISTER)
                value &= ~0x03;
                value |= mode;  
                self.bus.write_byte_data(self.address,MODE_REGISTER, value)
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break

    def getAxes(self):
        exc=0
        data = [0,128,0,128,0,128]
        while(True):
            data=[]
            try:
                #self.bus.write_byte(self.address,DATA_OUTPUT_REGISTER)
                for i in range(6):
                    data.append(self.bus.read_byte_data(self.address, DATA_OUTPUT_REGISTER+i))
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break
        
        x = data[1] | (data[0] << 8)
        if(x & (1 << (16 - 1))):
            x = x - (1<<16)

        y = data[5] | (data[4] << 8)
        if(y & (1 << (16 - 1))):
            y = y - (1<<16)

        z = data[3] | (data[2] << 8)
        if(z & (1 << (16 - 1))):
            z = z - (1<<16)

        self.compass_x = x
        self.compass_y = y
        self.compass_z = z
        return {"x": x, "y": y, "z": z}

    def getHeading(self):
        data = self.scalledReading()
        x=self.compass_x_scalled
        y=self.compass_y_scalled
        self.bearing = 0
        if(y>0): self.bearing = 90-(math.atan(x/y)) * 180/math.pi       
        if(y<0): self.bearing = 270-(math.atan(x/y)) * 180/math.pi
        if(y==0 and x<0): self.bearing = 180.0
        if(y==0 and x>0): self.bearing = 0.0
        #print ("degrees: " + str(self.bearing) + " x: "+ str(x) + " y: " + str(y))
        self.bearing = (self.bearing+180) % 360
        return self.bearing

    def scalledReading(self):
        self.getAxes()
        self.compass_x_scalled=self.compass_x*self.compass_gain_fact*self.compass_x_gainError+self.compass_x_offset
        self.compass_y_scalled=self.compass_y*self.compass_gain_fact*self.compass_y_gainError+self.compass_y_offset
        self.compass_z_scalled=self.compass_z*self.compass_gain_fact*self.compass_z_gainError+self.compass_z_offset

    def calibrate(self, select):
        if(select==1 or select==3):
            print("Calibrating the Magnetometer ........ Gain")
            self.compass_x_gainError = 0
            self.compass_y_gainError = 0
            self.compass_z_gainError = 0
            old_rega = self.bus.read_byte_data(self.address,CONFIG_REGISTER_A)
            self.bus.write_byte_data(self.address,CONFIG_REGISTER_A, 0x71)
            self.getAxes()
            while(self.compass_x<200 or self.compass_y<200 or self.compass_z<200):
                self.getAxes()

            self.compass_x_scalled = self.compass_x * self.compass_gain_fact
            self.compass_y_scalled = self.compass_y * self.compass_gain_fact
            self.compass_z_scalled = self.compass_z * self.compass_gain_fact

            self.compass_x_gainError = float(COMPASS_XY_EXCITATION)/self.compass_x_scalled
            self.compass_y_gainError = float(COMPASS_XY_EXCITATION)/self.compass_y_scalled
            self.compass_z_gainError = float(COMPASS_Z_EXCITATION)/self.compass_z_scalled

        
            #negative Bais

            self.bus.write_byte_data(self.address,CONFIG_REGISTER_A, 0x72)
            self.getAxes()
            while(self.compass_x>-200 or self.compass_y>-200 or self.compass_z>-200):
                self.getAxes()

            self.compass_x_scalled = self.compass_x * self.compass_gain_fact
            self.compass_y_scalled = self.compass_y * self.compass_gain_fact
            self.compass_z_scalled = self.compass_z * self.compass_gain_fact

            self.compass_x_gainError = float((COMPASS_XY_EXCITATION/math.fabs(self.compass_x_scalled))+self.compass_x_gainError)/2
            self.compass_y_gainError = float((COMPASS_XY_EXCITATION/math.fabs(self.compass_y_scalled))+self.compass_y_gainError)/2
            self.compass_z_gainError = float((COMPASS_Z_EXCITATION/math.fabs(self.compass_z_scalled))+self.compass_z_gainError)/2

            print("x_gain_offset: " + str(self.compass_x_gainError))
            print("y_gain_offset: " + str(self.compass_y_gainError))
            print("z_gain_offset: " + str(self.compass_z_gainError))
            
            self.bus.write_byte_data(self.address,CONFIG_REGISTER_A, old_rega)

            self.eeprom.writeFloat(EEPROM_X_GAIN_ERROR,self.compass_x_gainError)
            self.eeprom.writeFloat(EEPROM_Y_GAIN_ERROR,self.compass_y_gainError)
            self.eeprom.writeFloat(EEPROM_Z_GAIN_ERROR,self.compass_z_gainError)
            
        if(select==2 or select==3):
            print("Calibrating the Magnetometer ........ Offset")
            print("Please rotate the magnethometer with in one minute...")
            self.compass_x_offset = 0
            self.compass_y_offset = 0
            self.compass_z_offset = 0
            for i in (0,10):
                self.getAxes()

            x_max=-4000
            y_max=-4000
            z_max=-4000
            x_min=4000
            y_min=4000
            z_min=4000

            t=time.time()+900
            while(time.time()<t):
                self.getAxes()
                self.compass_x_scalled=float(self.compass_x)*self.compass_gain_fact*self.compass_x_gainError
                self.compass_y_scalled=float(self.compass_y)*self.compass_gain_fact*self.compass_y_gainError
                self.compass_z_scalled=float(self.compass_z)*self.compass_gain_fact*self.compass_z_gainError

                x_max = max(x_max, self.compass_x_scalled)
                y_max = max(y_max, self.compass_y_scalled)
                z_max = max(z_max, self.compass_z_scalled)

                x_min = min(x_min, self.compass_x_scalled)
                y_min = min(y_min, self.compass_y_scalled)
                z_min = min(z_min, self.compass_z_scalled)
                
            self.compass_x_offset = ((x_max-x_min)/2)-x_max
            self.compass_y_offset = ((y_max-y_min)/2)-y_max
            self.compass_z_offset = ((z_max-z_min)/2)-z_max
            print("xmax=" + str(x_max))
            print("ymax=" + str(y_max))
            print("zmax=" + str(z_max))
            print("xmin=" + str(x_min))
            print("ymin=" + str(y_min))
            print("zmin=" + str(z_min))
            print("Offset x=" + str(self.compass_x_offset))
            print("Offset y=" + str(self.compass_y_offset))
            print("Offset z=" + str(self.compass_z_offset))

            self.eeprom.writeFloat(EEPROM_X_OFFSET, self.compass_x_offset)
            self.eeprom.writeFloat(EEPROM_Y_OFFSET, self.compass_y_offset)
            self.eeprom.writeFloat(EEPROM_Z_OFFSET, self.compass_z_offset)
            
class HCM5883L_dummy:
    module_available = False
    def __init__(self, address = 0x1e):
        pass        
    def getAxes(self):
        return {"x": 128, "y": 128, "z": 128}

    def getHeading(self):
        return 180
