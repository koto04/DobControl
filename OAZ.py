#try:
    #from RPIO import PWM
#except ImportError:
#    pass

import board

class OAZ():

    module_available = True
    motor = None
    pwmPin = 0
    speed = 0
    speeds = [0.0,25.0,50.0,75.0,100.0]
    def __init__(self):
        self.motorIn = board.GPIO.PWM(board.GPIO_OAZ_IN, 50)
        self.motorOut = board.GPIO.PWM(board.GPIO_OAZ_OUT, 50)
        self.motorIn.stop()
        self.motorOut.stop()

    def move(self, speed):

        if speed == self.speed: return
        
        if speed>=0:
            pwm=self.speeds[speed]
            if(self.pwmPin==board.GPIO_OAZ_IN): 
                self.motorIn.stop()
                self.motorOut.start(pwm)
            elif(self.pwmPin==board.GPIO_OAZ_OUT):
                self.motorOut.ChangeDutyCycle(pwm)
            else:
                self.motorOut.start(pwm)                
            self.pwmPin = board.GPIO_OAZ_OUT            
        else:
            pwm=self.speeds[(speed*-1)]
            if(self.pwmPin==board.GPIO_OAZ_OUT): 
                self.motorOut.stop()
                self.motorIn.start(pwm)
            elif(self.pwmPin==board.GPIO_OAZ_IN):
                self.motorIn.ChangeDutyCycle(pwm)
            else:
                self.motorIn.start(pwm)                
            self.pwmPin = board.GPIO_OAZ_IN            

        self.speed = speed

    def stop(self):
        if(self.pwmPin==board.GPIO_OAZ_OUT): self.motorOut.stop()
        if(self.pwmPin==board.GPIO_OAZ_IN): self.motorIn.stop()
        self.speed=0
        self.pwmPin=0

class OAZ_dummy():

    module_available = False
    def __init__(self):
        pass
    def move(self, speed):
        pass
    def stop(self):
        pass        
        
        
