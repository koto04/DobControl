import time
import math
from at24c32 import EEPROM_X_STEPS, EEPROM_Y_STEPS, EEPROM_STEP_USTEPS, EEPROM_STEP_DRIVER
from at24c32 import AT24C32
import drivers.stepperDriver as stepdrv
import kivy.config as config

"""
USTEPS256 = 0x09 #1/256
USTEPS128 = 0x08 #1/128
USTEPS64 = 0x07 #1/64
USTEPS32 = 0x06 #1/32
USTEPS16 = 0x05 #1/16
USTEPS8 = 0x04 #1/8
USTEPS4 = 0x03 #1/4
USTEPS2 = 0x02 #1/2
USTEPS1 = 0x01 #1

USTEPS={}
USTEPS[0]=0
USTEPS[1]=1
USTEPS[2]=2
USTEPS[3]=4
USTEPS[4]=8
USTEPS[5]=16
USTEPS[6]=32
USTEPS[7]=64
USTEPS[8]=128
USTEPS[9]=256
"""

DRIVER_AZ_INDEX = 0
DRIVER_ALT_INDEX = 1

X_AXIS = 0
Y_AXIS = 1

"""
STEPS_X={}
STEPS_X[0] = 0x0000000000000000
STEPS_X[1] = 0x0000000000000001
STEPS_X[2] = 0x0101010101010101
STEPS_X[3] = 0x5555555555555555
STEPS_X[4] = 0xFFFFFFFFFFFFFFFF

STEPS_Y={}
STEPS_Y[0] = 0x0000000000000000
STEPS_Y[1] = 0x0000000000000001
STEPS_Y[2] = 0x0001000100010001
STEPS_Y[3] = 0x5555555555555555
STEPS_Y[4] = 0xFFFFFFFFFFFFFFFF
"""

class Control():
    module_available = True
    address = None
    config = 0x00
    
    eeprom = None
    
    driverType = stepdrv.DRIVER_TYPE_TMC2130
    driver = None
    
    ustepPreset = 0
    ustepAz = 0
    ustepAlt = 0
    
    xSteps360_16 = 0
    ySteps360_16 = 0
    
    uStepConfig = 0
    xStepResolution = 360.0/(200.0 * 8.0 * 10.0 * 3.0)
    yStepResolution = 360.0/(200.0 * 8.0 * 10.0 * 3.0)

    #correction factor to the measured steps
    xStepResolutionFaktor = 40#40/2 #40 tooth worm gear / 20 tooth gear
    yStepResolutionFaktor = 40#40/2 #40 tooth worm gear / 20 tooth gear

    mitFollowResolution = 0.00002

    def __init__(self, address=0x20):

        self.eeprom = AT24C32()
 
        self.ustepPreset = self.eeprom.readLong(EEPROM_STEP_USTEPS)
        print ("preset: " + str(self.ustepPreset))

        self.ustepAz = self.ustepPreset
        self.ustepAlt = self.ustepPreset
        
        
        self.readConfigFile()
        self.updateStepResolution()
        self.driver = stepdrv.StepperDriver.getDriver(self.driverType)

        print "driver type: " + str(self.driverType)
        print "driver: " + str(self.driver)
        
        
        self.driver.writeUstepConfig(self.ustepAz,self.ustepAlt,True)
        self.driver.writeStep(1,1)
    
    def readConfigFile(self):
        iniCfg = config.ConfigParser()
        iniCfg.read('dobcontrol.ini')
        self.driverType = int(iniCfg.get('control', 'stepper_driver'))
        
        xSteps360_16Raw = int(iniCfg.get('control', 'xSteps360_16'))
        ySteps360_16Raw = int(iniCfg.get('control', 'ySteps360_16'))
        
        self.xStepResolutionFaktor = float(iniCfg.get('control', 'xStepsTranslation'))
        self.yStepResolutionFaktor = float(iniCfg.get('control', 'yStepsTranslation'))
        
        self.mitFollowResolution = float(iniCfg.get('control', 'mitFollowResolution'))

        self.xSteps360_16 = xSteps360_16Raw * self.xStepResolutionFaktor
        self.ySteps360_16 = ySteps360_16Raw * self.yStepResolutionFaktor
    
    def updateStepResolution(self):
        
        #self.xStepResolution, self.yStepResolution = self.calculateStepResolutionByStepIndex(self.usteps)

        self.xStepResolution = self.calculateStepResolution(X_AXIS, self.ustepAz)
        self.yStepResolution = self.calculateStepResolution(Y_AXIS, self.ustepAlt)
        
        
    def calculateStepResolution(self, axis, usteps):
        
        if axis==X_AXIS: 
            if self.xSteps360_16==0:
                resolution = 360.0/(200.0 * 8.0 * 10.0 * 3.0)
            else:
                resolution = 360.0/self.xSteps360_16
        elif axis==Y_AXIS: 
            if self.ySteps360_16==0:
                resolution = 360.0/(200.0 * 8.0 * 10.0 * 3.0)
            else:
                resolution = 360.0/self.ySteps360_16
        
        factor = 16.0/usteps
        resolution *= factor
        return resolution
    
    """
    def calculateStepResolutionByStepIndex(self, uSteps):

        if self.xSteps360_16==0:
            xStepResolution = 360.0/(200.0 * 8.0 * 10.0 * 3.0)
        else:
            xStepResolution = 360.0/self.xSteps360_16

        if self.ySteps360_16==0:
            yStepResolution = 360.0/(200.0 * 8.0 * 10.0 * 3.0)
        else:
            yStepResolution = 360.0/self.ySteps360_16

        if(uSteps==USTEPS256):
            xStepResolution = (xStepResolution / 16.0)
            yStepResolution = (yStepResolution / 16.0)
        elif(uSteps==USTEPS128):
            xStepResolution = (xStepResolution / 8.0)
            yStepResolution = (yStepResolution / 8.0)
        elif(uSteps==USTEPS64):
            xStepResolution = (xStepResolution / 4.0)
            yStepResolution = (yStepResolution / 4.0)
        elif(uSteps==USTEPS32):
            xStepResolution = (xStepResolution / 2.0)
            yStepResolution = (yStepResolution / 2.0)
        elif(uSteps==USTEPS16):
            pass
        elif(uSteps==USTEPS8):
            xStepResolution = (xStepResolution * 2)
            yStepResolution = (yStepResolution * 2)
        elif(uSteps==USTEPS4):
            xStepResolution = (xStepResolution * 4)
            yStepResolution = (yStepResolution * 4)
        elif(uSteps==USTEPS2):
            xStepResolution = (xStepResolution * 8)
            yStepResolution = (yStepResolution * 8)
        elif(uSteps==USTEPS1):
            xStepResolution = (xStepResolution * 16)
            yStepResolution = (yStepResolution * 16)

        return xStepResolution, yStepResolution
    """
    
    def getResolution(self):
        return self.xStepResolution, self.yStepResolution
        
    def getDriver(self):
        return self.driver.driverType

    def setDriver(self, driver):
        self.driverType = driver
        self.eeprom.writeLong(EEPROM_STEP_DRIVER, self.driverType)
        
        self.driver = stepperDriver.StepperDriver.getDriver(self.driverType)
        steps = self.driver.getMaxUSteps()
        
        self.setUSteps(steps,steps)
        self.driver.writeStep(1,1)
    
    def setFollowUSteps(self):
        steps = self.driver.getMaxUSteps()
        
        while (steps>1):
            xRes = self.calculateStepResolution(X_AXIS, steps)
            yRes = self.calculateStepResolution(Y_AXIS, steps)

            if min(xRes, yRes) >  self.mitFollowResolution:
                break
            steps = steps>>1
        
        self.setUSteps(steps,steps)

    def setMaxUSteps(self):
        steps = self.driver.getMaxUSteps()
        self.setUSteps(steps, steps)
        return steps
    
    def restoreUstepPreset(self):

        self.setUSteps(self.ustepPreset,self.ustepPreset)

    def getPreset(self):
        return self.ustepPreset
    
    def updatePreset(self, usteps):
        print ("update preset: " + str(usteps))
        self.eeprom.writeLong(EEPROM_STEP_USTEPS, usteps)
        self.ustepPreset = usteps
        self.restoreUstepPreset()
        print ("preset updated: " + str(self.ustepPreset))
       
    def setUSteps(self, x_usteps, y_usteps):
        self.ustepAz = x_usteps
        self.ustepAlt = y_usteps
        self.driver.writeUstepConfig(self.ustepAz,self.ustepAlt)
        self.updateStepResolution()
        
                        
    def moveDegree(self, az, alt):

        #get steps count
        maxDriverUSteps = self.setMaxUSteps()
        minDriverUSteps = 1
        
        x = math.floor(az/self.xStepResolution)
        y = math.floor(alt/self.yStepResolution)
        
        ret = {"x": float(self.xStepResolution)*x, "y": float(self.yStepResolution)*y}
        
        #set direction
        self.driver.setDirectionAzAlt(x,y)
        
        #x index is 0, y index is 1
        
        stepCount=[0,0]
        stepCount[X_AXIS] = math.fabs(x)
        stepCount[Y_AXIS] = math.fabs(y)
        
        uStepInv = maxDriverUSteps
        while(max(stepCount)>0):
            usteps = maxDriverUSteps/uStepInv
            mx = math.floor(stepCount[X_AXIS]/uStepInv) 
            my = math.floor(stepCount[Y_AXIS]/uStepInv)
            self.setUSteps(usteps, usteps)
            stepCount[X_AXIS] -= mx*uStepInv
            stepCount[Y_AXIS] -= my*uStepInv
            uStepInv = uStepInv>>1
            self.moveFast(mx,my)
        
        self.restoreUstepPreset()
        return ret
        
    
    #moves given degree count in fast way, used by following
    def moveDegreeFast(self, az, alt):

        x = math.floor(az/self.xStepResolution)
        y = math.floor(alt/self.yStepResolution)
        ret = {"x": float(self.xStepResolution)*x, "y": float(self.yStepResolution)*y}
        self.driver.setDirectionAzAlt(x,y)
        xsteps = math.fabs(x)
        ysteps = math.fabs(y)
        self.moveFast(xsteps, ysteps)
                    
        return ret

    def moveFast(self, xsteps, ysteps):
        while xsteps>0 or ysteps>0:
            azStep=0
            altStep=0
            if(xsteps>0):
                azStep = 1
                xsteps -= 1
            if(ysteps>0):
                altStep = 1
                ysteps -= 1
            self.driver.writeStep(azStep,altStep)
            
                        
    def moveRaw(self,x,y):
        dx = 0
        dy = 0

        x_abs = math.fabs(x)
        y_abs = math.fabs(y)
        
        xstep = (x_abs*(1<<x_abs)-1) if x!=0 else 0
        #xstep = STEPS_X[math.fabs(x)]
        ystep = (y_abs*(1<<y_abs)-1) if y!=0 else 0
        #ystep = STEPS_Y[math.fabs(y)]

        if x==0 and y==0: return {"dx":0, "dy":0}
        
        self.driver.setDirectionAzAlt(x,y)
        for i in range(0,64):
            azStep = xstep&0x01
            altStep = ystep&0x01
            self.driver.writeStep(azStep,altStep)
            dx+=azStep
            dy+=altStep
            xstep = xstep>>1
            ystep = ystep>>1
        
        if(x<0): dx*=-1
        if(y<0): dy*=-1
        ret = {"dx": dx, "dy": dy}
        return ret
        
    def moveStep(self, x ,y):

        if x==0 and y==0: return {"x":0, "y":0}
        
        azStep= 1 if x!=0 else 0
        altStep= 1 if y!=0 else 0
        
        maxDriverUSteps = self.driver.getMaxUSteps()
        
        xAbs = int(math.fabs(x))
        yAbs = int(math.fabs(y))
        self.driver.setDirectionAzAlt(x,y)
        
        xUStep = min(self.ustepPreset<<(4-xAbs),maxDriverUSteps) 
        yUStep = min(self.ustepPreset<<(4-yAbs),maxDriverUSteps)
        
        self.setUSteps(xUStep, yUStep)
        
        xRes = self.xStepResolution * azStep * (1 if x>0 else -1)
        yRes = self.yStepResolution * altStep * (1 if y>0 else -1)

        for i in range(0,32):
            self.driver.writeStep(azStep,altStep)
            
        ret = {"x": 32*xRes, "y": 32*yRes}
        
        return ret
    
class Control_dummy():

    driverType = stepdrv.DRIVER_TYPE_BIGEASYDRIVER
    module_available = False
    
    def __init__(self, address = 0x20):

        pass
    def setUSteps(self, x_usteps, y_usteps):
        pass
    def getUSteps(self):
        return 16
    def setDriver(self, driver):
        self.driverType = driver
    def getDriver(self):
        return self.driverType
        
    def readStepsFromEeprom(self):
        pass
    def moveDegree(self, az, alt):

        ret = {"x": az, "y": alt}
        return ret
    def moveDegreeFast(self, az, alt):

        ret = {"x": az, "y": alt}
        return ret
    def moveBits(self, xstep, ystep):
        pass                
    def calcDelta(self, x, y):

        ret = {"x": 30, "y": 30}
        return ret
        
    def moveRaw(self,x,y):
        ret = {"dx": 10, "dy": 10}
        return ret
        
    def moveStep(self, x ,y):

        ret = {"x": 10, "y": 10}
        
        return ret
    def setUStepConfig(self):
        pass
       
    def getDriver(self):
        return self.driverType

    def setDriver(self, driver):
        self.driverType = driver
    
    def setMaxUSteps(self):
        pass

