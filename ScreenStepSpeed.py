from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button 

from kivy.properties import ObjectProperty
import drivers.stepperDriver as stepdrv

class ScreenStepSpeed(Screen):

    app = ObjectProperty()
    control = None
    ustep_items = ObjectProperty()
    
    def on_enter(self, *args):
        Screen.on_enter(self, *args)
        self.control = self.app.control
        self.initUStepButtons()

    def initUStepButtons(self):
        self.ustep_items.clear_widgets()
        if(self.control.getDriver() == stepdrv.DRIVER_TYPE_BIGEASYDRIVER):
            self.ustep_items.add_widget(Button(text="1/16 Step", on_release=lambda a: self.setUSteps(16)))
            self.ustep_items.add_widget(Button(text="1/8 Step", on_release=lambda a: self.setUSteps(8)))
            self.ustep_items.add_widget(Button(text="1/2 Step", on_release=lambda a: self.setUSteps(2)))
            
        elif(self.control.getDriver()==stepdrv.DRIVER_TYPE_RAPS128):
            self.ustep_items.add_widget(Button(text="1/128 Step", on_release=lambda a:self.setUSteps(128)))
            self.ustep_items.add_widget(Button(text="1/64 Step", on_release=lambda a:self.setUSteps(64)))
            self.ustep_items.add_widget(Button(text="1/32 Step", on_release=lambda a:self.setUSteps(32)))
            self.ustep_items.add_widget(Button(text="1/16 Step", on_release=lambda a:self.setUSteps(16)))
            self.ustep_items.add_widget(Button(text="1/8 Step", on_release=lambda a:self.setUSteps(8)))
            self.ustep_items.add_widget(Button(text="1/4 Step", on_release=lambda a:self.setUSteps(4)))
            self.ustep_items.add_widget(Button(text="1/2 Step", on_release=lambda a:self.setUSteps(2)))
            self.ustep_items.add_widget(Button(text="1/1 Step", on_release=lambda a:self.setUSteps(1)))

        elif(self.control.getDriver()==stepdrv.DRIVER_TYPE_TMC2130):
            self.ustep_items.add_widget(Button(text="1/256 Step", on_release=lambda a:self.setUSteps(256)))
            self.ustep_items.add_widget(Button(text="1/128 Step", on_release=lambda a:self.setUSteps(128)))
            self.ustep_items.add_widget(Button(text="1/64 Step", on_release=lambda a:self.setUSteps(64)))
            self.ustep_items.add_widget(Button(text="1/32 Step", on_release=lambda a:self.setUSteps(32)))
            self.ustep_items.add_widget(Button(text="1/16 Step", on_release=lambda a:self.setUSteps(16)))
            self.ustep_items.add_widget(Button(text="1/8 Step", on_release=lambda a:self.setUSteps(8)))
            self.ustep_items.add_widget(Button(text="1/4 Step", on_release=lambda a:self.setUSteps(4)))
            self.ustep_items.add_widget(Button(text="1/2 Step", on_release=lambda a:self.setUSteps(2)))
            self.ustep_items.add_widget(Button(text="1/1 Step", on_release=lambda a:self.setUSteps(1)))

    def setUSteps(self, uSteps):
        self.control.updatePreset(uSteps)
        self.manager.current = self.app.previousScreen
