from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.modalview import ModalView
from kivy.properties import StringProperty, ObjectProperty
import os
from kivy.uix.screenmanager import ScreenManager, Screen

class PopupMessage(ModalView):

    app = ObjectProperty()
    messageText = StringProperty()
    
    def showText(self, text):
        self.messageText = text
        self.open()
                
class PopupMessageYesNo(ModalView):

    app = ObjectProperty()

    buttonYes = None
    buttonNo = None
    
    messageText = StringProperty()
    
    def on_open(self):
        ModalView.on_open(self)

    def on_dismiss(self, *args):
        self.button_list.clear_widgets()

    def showText(self, text, onYes=None, onNo=None):
        self.buttonYes = Button(text='Yes')
        self.buttonNo = Button(text='No')

        self.button_list.add_widget(self.buttonYes)
        self.button_list.add_widget(self.buttonNo)

        self.messageText = text

        if not onYes is None:
            self.buttonYes.bind(on_release=onYes)
        self.buttonYes.bind(on_release=self.nop)
        
        if not onNo is None:
            self.buttonNo.bind(on_release=onNo)
        self.buttonNo.bind(on_release=self.nop)
            
        self.open()

    def nop(self, *args):
        self.dismiss()
