# -*- coding: utf-8 -*-
'''
Created on 31.10.2015

@author: martin
'''

from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.label import Label 
from kivy.clock import Clock, mainthread
from functools import partial
from time import strftime, localtime

import math
import time

class ScreenKoordGoto(Screen):
    
    valueInput = StringProperty('')
    formattedInput = StringProperty('')
    rektaszension = StringProperty('')
    deklination = StringProperty('')
    sign = ""
    rekValue = 0
    dekValue = 0
    

    app = ObjectProperty()

    calculator = None
    control = None

    moving = False
    
    menubar = ObjectProperty()
    coordInput = ObjectProperty()
    menu_page = 1

    value = 0
    value_formatted = ""

    def __init__(self, **kwargs):
        super(ScreenKoordGoto, self).__init__(**kwargs)

        self.calculator = self.app.calculator
        self.control = self.app.control
        
        self.buttonGoto = Button(text="Goto", id="btnGoto", on_release=self.gotoPosition, disabled=True)
        self.buttonDummy1 = Button(text="", id="btnDummy1")
        self.buttonDummy2 = Button(text="", id="btnDummy2")
        self.buttonBack = Button(text="Back", id="btnBack", on_release=self.app.screenControl)

        btn=Button(text="7", on_release=partial(self.onInput, value="7"))
        self.coordInput.add_widget(btn)
        btn=Button(text="8", on_release=partial(self.onInput, value="8"))
        self.coordInput.add_widget(btn)
        btn=Button(text="9", on_release=partial(self.onInput, value="9"))
        self.coordInput.add_widget(btn)
        btn=Button(text="Rek", on_release=partial(self.onInput, value="Rek"))
        self.coordInput.add_widget(btn)

        btn=Button(text="4", on_release=partial(self.onInput, value="4"))
        self.coordInput.add_widget(btn)
        btn=Button(text="5", on_release=partial(self.onInput, value="5"))
        self.coordInput.add_widget(btn)
        btn=Button(text="6", on_release=partial(self.onInput, value="6"))
        self.coordInput.add_widget(btn)
        btn=Button(text="Dek", on_release=partial(self.onInput, value="Dek"))
        self.coordInput.add_widget(btn)

        btn=Button(text="1", on_release=partial(self.onInput, value="1"))
        self.coordInput.add_widget(btn)
        btn=Button(text="2", on_release=partial(self.onInput, value="2"))
        self.coordInput.add_widget(btn)
        btn=Button(text="3", on_release=partial(self.onInput, value="3"))
        self.coordInput.add_widget(btn)
        btn=Button(text="Clear", on_release=partial(self.onInput, value="Clear"))
        self.coordInput.add_widget(btn)

        btn=Label(text="")
        self.coordInput.add_widget(btn)
        btn=Button(text="0", on_release=partial(self.onInput, value="0"))
        self.coordInput.add_widget(btn)
        btn=Button(text="+/-", on_release=partial(self.onInput, value="Sign"))
        self.coordInput.add_widget(btn)
        btn=Button(text="C", on_release=partial(self.onInput, value="C"))
        self.coordInput.add_widget(btn)
        
    def on_enter(self, *args):

        self.menu_page=0
        self.refreshMenu()
        
        self.buttonGoto.disabled=True
        
    def on_leave(self, *args):
        pass
    def toggleMenu(self, *args):
        self.menu_page = 0
        self.refreshMenu()
        
    def refreshMenu(self, *args):
        self.menubar.clear_widgets()
        if(self.menu_page==0):
            self.menubar.add_widget(self.buttonGoto)
            self.menubar.add_widget(self.buttonDummy1)
            self.menubar.add_widget(self.buttonDummy2)
            self.menubar.add_widget(self.buttonBack)
        
    def onInput(self, *args, **kwargs):
        v=kwargs["value"]
        if(v=='Clear'):
            self.valueInput = ""
            self.sign = ""
            self.rektaszension = ""
            self.deklination = ""
            self.checkInput()

        elif(v=="0") or (v=="1") or (v=="2") or (v=="3") or (v=="4") or (v=="5") or (v=="6") or (v=="7") or (v=="8") or (v=="9"):
            if(len(self.valueInput)<6):
                self.valueInput += v
        elif(v=="Sign"):
            if(self.sign=="-"):
                self.sign=""
            else:
                self.sign="-"
        elif(v=="Rek"):
            if(len(self.valueInput)==6):
                if(self.sign=="-"):
                    return
                if(int(self.valueInput[0:2])>=24):
                    return
                if(int(self.valueInput[2:4])>=60):
                    return
                if(int(self.valueInput[4:6])>=60):
                    return              
                self.rekValue = float(self.valueInput[0:2]) + (float(self.valueInput[2:4])/60) + (float(self.valueInput[4:6])/3600)
                self.rektaszension = self.valueInput[0:2] + "h " + self.valueInput[2:4] + "m " + self.valueInput[4:6] + "s"
                self.valueInput = ""
                self.sign=""
                self.checkInput()

        elif(v=="Dek"):
            if(len(self.valueInput)==6):
                if(int(self.valueInput[0:2])>=90):
                    return
                if(int(self.valueInput[2:4])>=60):
                    return
                if(int(self.valueInput[4:6])>=60):
                    return
                   
                self.dekValue = (float(self.valueInput[0:2])) + (float(self.valueInput[2:4])/60) + (float(self.valueInput[4:6])/3600)
                if(self.sign)=="-":
                    self.dekValue *= -1
                self.deklination = self.sign + self.valueInput[0:2] + "° " + self.valueInput[2:4] + "' " + self.valueInput[4:6] + "\""
                self.valueInput = ""
                self.sign=""
                self.checkInput()
                
        elif(v=="C"):
            if(len(self.valueInput)>0):
                self.valueInput = self.valueInput[:-1]
        
        self.formatInput()

    def checkInput(self):
        self.buttonGoto.disabled = True
        if (self.rektaszension!="") and (self.deklination!=""):
            if self.calculator.isVisibleKoord(self.rekValue, self.dekValue):
                self.buttonGoto.disabled = False
                
    def formatInput(self):
        self.formattedInput = self.sign
        for i in range(0,len(self.valueInput),2):
            self.formattedInput += self.valueInput[i:i+2] + " "
            
        
    def gotoPosition(self, *args, **kwargs):
        for i in range(0,4):
            horPos = self.app.calculator.PositionHorizontal(self.rekValue, self.dekValue)
            c = self.app.dobAz
            z = horPos[0]-c
            alt = horPos[1]-self.app.dobAlt
            if z<0: z+=360
            if z>180: z-=360
            move = self.control.moveDegree(z,alt)
            self.app.dobAz += move["x"]
            self.app.dobAlt += move["y"]
            if(self.app.dobAz<0): self.app.dobAz+=360
            if(self.app.dobAlt<0): self.app.dobAlt+=360
            if(self.app.dobAz>360): self.app.dobAz-=360
            if(self.app.dobAlt>360): self.app.dobAlt-=360

        aeqPos = self.calculator.PositionAequatorial(self.app.dobAz, self.app.dobAlt)
        self.app.rektaszension = aeqPos[0]
        self.app.deklination = aeqPos[1]
        
        
