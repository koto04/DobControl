
import math
import csv
import os, sys
from KoordCalculation import KoordCalculation
import GitRepositoryUtils as internet
import requests
import datetime

class Constellation:
    constellations = []
    constellation_id = ''
    name_lat = ''
    name_ger = ''
    ra = ''
    de = ''
    radeg = 0
    dedeg = 0
    scale = ''
    object_type = ''
    
    @staticmethod
    def loadFile():
        Constellation.constellations = []
        cr = csv.reader(open(os.path.dirname(os.path.abspath(__file__)) + "/Catalog.csv","rb"), delimiter=",")
        for row in cr:
            if row[10] == 'Constellation':
                Constellation.constellations.append(Constellation(row[1], row[2], row[6], row[3], row[4], row[7])) 
        Constellation.constellations.sort(key=lambda x: x.constellation_id)

    @staticmethod
    def getConstellations():
        return Constellation.constellations

    def __init__(self, constellation_id='', name_lat='', name_ger='', ra='', de='', scale=''):
        self.constellation_id = constellation_id
        self.name_lat = name_lat
        self.name_ger = name_ger
        self.ra = float(ra)
        self.de = float(de)
        self.radeg = ra
        self.dedeg = de

    
class CatalogObject:

    TYPE_CONSTELLATION = 'Constellation'
    TYPE_CONSTELLATION_OBJECTS = 'ConstellationObjects'
    TYPE_STAR = 'Star'
    TYPE_CLUSTER = 'Cluster'
    TYPE_NEBULA = 'Nebula'
    TYPE_MESSIER = 'Messier'
    TYPE_GALAXY = 'Galaxy'
    TYPE_PLANET = 'planet'
    TYPE_SATELLITE = 'satellite'
    
    typ = ''
    object_id = ''
    name = ''
    ra = ''
    ra_h = 0
    de = ''
    radeg = 0
    dedeg = 0
    mag = 0
    param1 = ''     #Messier/Spektrum/Constellation name/planet diameter
    param2 = ''     #Constellation/planet orbital period
    param3 = ''     #Constellation star/Constellation name/planet phase
    param4 = ''     #Calibration star
    image = ''
    distance = ''
    delta = 0.0
    object_type = ''
    objects = []
    
    hasPlanets = False
    
    @staticmethod
    def loadFile():
        CatalogObject.objects = []
        i=0
        cr = csv.reader(open(os.path.dirname(os.path.abspath(__file__)) + "/Catalog.csv","rb"), delimiter=",")
        for row in cr:
            if row[10] != 'Constellation':
                CatalogObject.objects.append(CatalogObject(row[0], row[1], row[2], row[3], row[4], row[5],row[6],row[7],row[8], row[9],row[10]))
                i+=1
        print str(i) + " objects added"
        posSun = KoordCalculation.SunPosition()
        print "sun position" + str(posSun)
        CatalogObject.objects.append(CatalogObject(CatalogObject.TYPE_STAR, "HD0", "Sun", str(posSun[0]), str(posSun[1]), "-26.74", "G2V", "-","-","1",CatalogObject.TYPE_STAR))


    @staticmethod
    def loadOnlineObjects():
        if internet.internetAvailable() == True:
            CatalogObject.hasPlanets=True
            CatalogObject.addImcceObject('p:sun')
            CatalogObject.addImcceObject('p:mercury')
            CatalogObject.addImcceObject('p:venus')
            CatalogObject.addImcceObject('p:mars')
            CatalogObject.addImcceObject('p:jupiter')
            CatalogObject.addImcceObject('p:saturn')
            CatalogObject.addImcceObject('p:uranus')
            CatalogObject.addImcceObject('p:neptune')
            CatalogObject.addImcceObject('p:moon')


    @staticmethod
    def addImcceObject(name):
        uri = 'http://vo.imcce.fr'
        path = '/webservices/miriade/ephemcc_query.php'
        params = (
                    ('-ep',datetime.datetime.utcnow().isoformat()),
                    ('-name',name),
                    ('-nbd','1'),
                    ('-tscale','UTC'),
                    ('-observer','@500'),
                    ('-theory','INPOP'),
                    ('-teph','1'),
                    ('-tcoor','1'),
                    ('-mime','json'),
                    ('-output','--jd'),
        			('-from','MiriadeDoc'),
        )
        fullPath = uri + path 
        response = requests.get(url=fullPath, params=params)
        if (response.status_code!=200):
            return
        data = response.json()
        sso = data["sso"]
        sso_data = data["data"][0]

        CatalogObject.objects.append(CatalogObject(sso["type"], sso["name"], sso["name"], str(float(sso_data["ra"])), str(float(sso_data["dec"])), str(round(float(sso_data["vmag"]),3)), str(round(float(sso["parameters"]["diameter"]),2)) + " km", str(float(sso["parameters"]["orbital_period"])) + " days",str(round(float(sso_data["phase"]),4)),"1",sso["type"]))

    @staticmethod
    def getObjects(object_type, name=''):
        ret = []
        if object_type==CatalogObject.TYPE_MESSIER:
            for o in CatalogObject.objects:
                if o.param1 != '' and o.object_type != CatalogObject.TYPE_STAR and o.object_type != CatalogObject.TYPE_PLANET:
                    ret.append(o)
            ret.sort(key=lambda x: x.param1)
        elif object_type==CatalogObject.TYPE_STAR:
            for o in CatalogObject.objects:
                if o.object_type == CatalogObject.TYPE_STAR:
                    ret.append(o)
            ret.sort(key=lambda x: x.name)
        elif object_type==CatalogObject.TYPE_CONSTELLATION_OBJECTS:
            for o in CatalogObject.objects:
                if o.param2 == name:
                    ret.append(o)
            ret.sort(key=lambda x: x.name)
        elif object_type==CatalogObject.TYPE_PLANET:
            for o in CatalogObject.objects:
                if o.object_type ==CatalogObject.TYPE_PLANET or o.object_type == CatalogObject.TYPE_SATELLITE:
                    ret.append(o)
            ret.sort(key=lambda x: x.name)
        else:
            for o in CatalogObject.objects:
                if o.object_type == object_type:
                    ret.append(o)
            ret.sort(key=lambda x: x.object_id)
                
        return ret

    @staticmethod
    def findObject(ra_h, de):
        ra = ra_h / 24 * 360
        d = 361.0
        ret = None
        for o in CatalogObject.objects:
            o.calcDistance(ra, de)
            if(d>o.delta):
                d=o.delta
                ret=o
        return ret
        
    @staticmethod
    def getCalibrationStars():
        stars = {}
        for o in CatalogObject.objects:
            if((o.object_type=="Star" or o.object_type=="planet") and o.param4=='1'):
                stars.update({o.name: [o.ra_h, o.dedeg, o.object_id]})
        return stars    

    def __init__(self, typ='', object_id='', name='', ra='', de='', mag='',param1='',param2='', param3='', param4='', object_type=''):
        self.name = name
        self.distance = ''
        self.radeg = float(ra)
        self.dedeg = float(de)
        self.ra_h = (self.radeg/360.0)*24.0
        self.typ = typ
        self.mag = mag
        self.object_id = object_id
        self.param1 = param1
        self.param2 = param2
        self.param3 = param3
        self.param4 = param4
        self.object_type = object_type
        self.short_info = ""
        type_words = self.typ.split(" ")
        for tw in type_words:
            self.short_info += tw.upper()[0:1]
            
        self.short_info += " " + str(self.mag)
            

    def calcDistance(self, ra, de):

        d=acos(sin(self.radeg)*sin(ra)+cos(self.radeg)*cos(ra)*cos(de-self.dedeg))
        self.delta = d
        return self.delta

def sin(v):
    return math.sin(math.radians(v))
def cos(v):
    return math.cos(math.radians(v))
def tan(v):
    return math.tan(math.radians(v))
def asin(v):
    return math.degrees(math.asin(v))
def acos(v):
    return math.degrees(math.acos(v))
def atan(v):
    return math.degrees(math.atan(v))
                    
