from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.modalview import ModalView
from kivy.uix.image import Image
from kivy.properties import StringProperty, ObjectProperty
from CatalogObject import CatalogObject
import os
from functools import partial
from kivy.uix.screenmanager import ScreenManager, Screen

class PopupObjectInfo(ModalView):

    row1 = StringProperty()
    row2 = StringProperty()
    row3 = StringProperty()
    row4 = StringProperty()
    row5 = StringProperty()

    goto_callback = None
    PHOTO = 'Object'
    LOCATION = 'Location'
    image = ''

    app = ObjectProperty()

    o = None

    btnImage =  None
        
    def ShowInfo(self, o):
        self.o = o
        self.InitInfo(o)
        
        if os.path.isfile(os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + ".jpg") and os.path.isfile(os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + "_location.jpg"):

            self.btnImage = Button(text="Location",)
            self.btnImage.bind(on_release=partial(self.ChangeImage, o))
            self.button_box.add_widget(self.btnImage)

        else:
            self.btnImage = Button(text="")
            self.button_box.add_widget(self.btnImage)

        btn_ok = Button(text='OK')
        btn_ok.bind(on_release=self.dismiss)
        self.button_box.add_widget(btn_ok)

        self.open()
        
    def ShowCatalogObject(self, o, goto_callback=None):
        if self.app.is_calibrated() and self.app.calculator.isVisible(o):
            btn = Button(text="GoTo")
            btn.bind(on_release=self.GoTo)
        else:
            btn = Button(text="")
    
        self.button_box.add_widget(btn)
        o.calcDistance(self.app.rektaszension, self.app.deklination)
        
        self.goto_callback = goto_callback
        self.ShowInfo(o)

    def GoTo(self, *args):
        for i in range(0,4):
            horPos = self.app.calculator.PositionHorizontal(self.o.ra_h, self.o.dedeg)
            c = self.app.dobAz
            z = horPos[0]-c
            alt = horPos[1]-self.app.dobAlt
            if z<0: z+=360
            if z>180: z-=360
            move = self.app.control.moveDegree(z,alt)
            self.app.dobAz += move["x"]
            self.app.dobAlt += move["y"]
            if(self.app.dobAz<0): self.app.dobAz+=360
            if(self.app.dobAlt<0): self.app.dobAlt+=360
            if(self.app.dobAz>360): self.app.dobAz-=360
            if(self.app.dobAlt>360): self.app.dobAlt-=360

        aeqPos = self.app.calculator.PositionAequatorial(self.app.dobAz, self.app.dobAlt)
        self.app.rektaszension = aeqPos[0]
        self.app.deklination = aeqPos[1]
        
        self.dismiss()
        if(self.goto_callback != None): self.goto_callback()
        
        
    def ChangeImage(self, *args, **kwargs):
        o=args[0]
        if(self.image==self.PHOTO):
            self.btnImage.text = 'Object'
            self.object_image.clear_widgets()
            obj_image = Image(source=os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + "_location.jpg", keep_ratio=True)
            if(self.app.nightMode==True):
                obj_image.color = [1,0,0,1]
            self.object_image.add_widget(obj_image)
            self.image = self.LOCATION
        elif(self.image==self.LOCATION):
            self.btnImage.text = 'Location'
            self.object_image.clear_widgets()
            obj_image = Image(source=os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + ".jpg", keep_ratio=True)
            if(self.app.nightMode==True):
                obj_image.color = [1,0,0,1]
            self.object_image.add_widget(obj_image)
            self.image = self.PHOTO
            
        
    def InitInfo(self, o):
        if(o.typ == CatalogObject.TYPE_STAR):
            self.row1 = o.name
            self.row2 = o.object_id
            self.row3 = "Magnitude: " + str(o.mag)
            self.row4 = "Spectrum: " + str(o.param1)
            self.row5 = "Pos delta: " + str(round(o.delta,4))
        elif(o.typ == CatalogObject.TYPE_PLANET):
            self.row1 = o.name
            self.row2 = "Diameter: " + str(o.param1)
            self.row3 = "Magnitude: " + str(o.mag)
            self.row4 = "Phase: " + str(o.param3)
            self.row5 = "Pos delta: " + str(round(o.delta,4))
            
        else:
            self.row1 = o.object_id + " " + o.name
            if(o.param1!=""):
                self.row1 += " (" + o.param1 + ")"
            self.row2 = o.typ
            self.row3 = "Magnitude: " + str(o.mag)
            self.row4 = "Constellation: " + str(o.param2)
            self.row5 = "Pos delta: " + str(round(o.delta,4))
        if(os.path.isfile(os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + ".jpg")):
            obj_image = Image(source=os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + ".jpg", keep_ratio=True)
            if(self.app.nightMode==True):
                obj_image.color = [1,0,0,1]
            self.object_image.add_widget(obj_image)
            self.image = self.PHOTO
        elif(os.path.isfile(os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + "_location.jpg")):
            obj_image = Image(source=os.path.dirname(os.path.abspath(__file__)) + "/objects/" + o.object_id + "_location.jpg", keep_ratio=True)
            if(self.app.nightMode==True):
                obj_image.color = [1,0,0,1]
            self.object_image.add_widget(obj_image)
            self.image = self.LOCATION
        

    def on_leave(self, *args):
        self.object_image.clear_widgets()
        self.button_box.clear_widgets()
        
