# ADXL345 Python library for Raspberry Pi 
#
# author:  Jonathan Williamson
# license: BSD, see LICENSE.txt included in this package
# 
# This is a Raspberry Pi Python implementation to help you get started with
# the Adafruit Triple Axis ADXL345 breakout board:
# http://shop.pimoroni.com/products/adafruit-triple-axis-accelerometer

from time import sleep
from collections import deque
import sys
# select the correct i2c bus for this revision of Raspberry Pi



# ADXL345 constants
EARTH_GRAVITY_MS2   = 9.80665
SCALE_MULTIPLIER    = 0.004

DATA_FORMAT         = 0x31
BW_RATE             = 0x2C
POWER_CTL           = 0x2D

BW_RATE_1600HZ      = 0x0F
BW_RATE_800HZ       = 0x0E
BW_RATE_400HZ       = 0x0D
BW_RATE_200HZ       = 0x0C
BW_RATE_100HZ       = 0x0B
BW_RATE_50HZ        = 0x0A
BW_RATE_25HZ        = 0x09

RANGE_2G            = 0x00
RANGE_4G            = 0x01
RANGE_8G            = 0x02
RANGE_16G           = 0x03

MEASURE             = 0x08
AXES_DATA           = 0x32

BIT_10              = 0x0A

POWER_SAVE_DISABLE  = 0x08

class ADXL345:
    module_available = True
    bus = None
    address = None

    xValues = deque([],maxlen=10)
    yValues = deque([],maxlen=10)
    zValues = deque([],maxlen=10)

    def __init__(self, address = 0x53):
        smbus=__import__('smbus')
        self.bus = smbus.SMBus(1)
        self.address = address
        self.setConfig()

    def setConfig(self):
        self.bus.write_byte_data(self.address, BW_RATE, BIT_10)
        self.bus.write_byte_data(self.address, POWER_CTL, MEASURE)
        self.bus.write_byte_data(self.address, DATA_FORMAT, 0x08)
        sleep(0.5)
                
    # returns the current reading from the sensor for each axis
    #
    def getAxes(self):
        exc=0
        data = []
        while (True):
            data = []
            try:
                for i in range(6):
                    data.append(self.bus.read_byte_data(self.address,(AXES_DATA+i)))
                break
            except:
                print "exce"
                sleep(.2)
                exc+=1
        if(exc>=5):
            self.outputMessage(sys.exc_info()[0])
            return {"x": 128, "y": 128, "z": 128}
            
        xAccl = ((data[1] & 0x03) * 256) + data[0]
        if xAccl > 511 :
            xAccl -= 1024
            
        yAccl = ((data[3] & 0x03) * 256) + data[2]
        if yAccl > 511 :
            yAccl -= 1024

        zAccl = ((data[5] & 0x03) * 256) + data[4]
        if zAccl > 511 :
            zAccl -= 1024

        self.xValues.append(xAccl)
        self.yValues.append(yAccl)
        self.zValues.append(zAccl)

        x = sum(self.xValues)/len(self.xValues)
        y = sum(self.yValues)/len(self.yValues)
        z = sum(self.zValues)/len(self.zValues)
        
        return {"x": x, "y": y, "z": z}


class ADXL345_dummy:
    module_available = False
    def __init__(self, address = 0x53):
        pass
    
    def getAxes(self):
        return {"x": 128, "y": 128, "z": 128}


"""
a = ADXL345()

while(True):
    data = a.getAxes()
    print "x: " + '{:02X}'.format(data["x"]) + ", y: " + '{:02X}'.format(data["y"]) + ", z: " + '{:02X}'.format(data["z"])
    sleep(.2)
"""    