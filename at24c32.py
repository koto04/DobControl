# reserved 
# 0x0100 - 0x0114 compass
# 0x0200 - 0x0210 level
import time, sys
import struct
import binascii
from PopupMessage import PopupMessage

#EEPROM config addresses
EEPROM_X_GAIN_ERROR = 0x0100
EEPROM_Y_GAIN_ERROR = 0x0104
EEPROM_Z_GAIN_ERROR = 0x0108
EEPROM_X_OFFSET = 0x010C
EEPROM_Y_OFFSET = 0x0110
EEPROM_Z_OFFSET = 0x0114

EEPROM_X_STEPS = 0x0120
EEPROM_Y_STEPS = 0x0124
EEPROM_STEP_USTEPS = 0x0128
EEPROM_STEP_DRIVER = 0x0132

EEPROM_LEVEL_XOFFSET = 0x200
EEPROM_LEVEL_YOFFSET = 0x204

class AT24C32:
    module_available = True
    bus = None
    address = None

    def __init__(self, address=0x50):
        smbus=__import__('smbus')
        self.bus = smbus.SMBus(1)
        self.address = address

    def writeFloat(self, address, value):
        ba = []
        exc=0
        for c in struct.pack(">hf",address,value):
            ba.append(ord(c))
        while(True):
            try:
                self.bus.write_i2c_block_data(self.address, ba[0], ba[1:])
                time.sleep(0.01)
                break
            except:
                time.sleep(0.2)
                exc +=1

            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break
            
        
    def readFloat(self, address):

        exc=0        
        while(True):
            try:
                self.bus.write_byte_data(self.address,(address>>8), (address&0xff))
                value=""
                time.sleep(0.01)
                value+=chr(self.bus.read_byte(self.address))
                value+=chr(self.bus.read_byte(self.address))
                value+=chr(self.bus.read_byte(self.address))
                value+=chr(self.bus.read_byte(self.address))
                return struct.unpack(">f",value)[0]
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break
        return 0

    def writeLong(self, address, value):
        exc=0        
        while(True):
            ba = []
            try:
                for c in struct.pack(">hl",address,value):
                    ba.append(ord(c))
                self.bus.write_i2c_block_data(self.address, ba[0], ba[1:])
                time.sleep(0.01)
                break
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break
        
    def readLong(self, address):
        exc=0        
        while(True):
            try:
                self.bus.write_byte_data(self.address,(address>>8), (address&0xff))
                value=""
                time.sleep(0.01)
                value+=chr(self.bus.read_byte(self.address))
                value+=chr(self.bus.read_byte(self.address))
                value+=chr(self.bus.read_byte(self.address))
                value+=chr(self.bus.read_byte(self.address))
                return struct.unpack(">l",value)[0]
            except:
                time.sleep(0.2)
                exc +=1
            if(exc>=5):
                self.outputMessage(sys.exc_info()[0])
                break
        return 0

    def outputMessage(self, message):
        pass
        #pm = PopupMessage()
        #pm.showText(self.__class__.__name__ + ": " + str(message))
        

class AT24C32_dummy:
    module_available = False
    def __init__(self, address=0x50):
        pass

    def writeFloat(self, address, value):
        pass
        
    def readFloat(self, address):
        return 10

    def writeLong(self, address, value):
        pass
        
    def readLong(self, address):                
        return 10

    def outputMessage(self, message):
        pass
        #pm = PopupMessage()
        #pm.showText(self.__class__.__name__ + ": " + str(message))
