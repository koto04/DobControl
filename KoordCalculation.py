# -*- coding: utf-8 -*-
'''
Created on 04.02.2015

@author: martin
'''

from datetime import datetime
import math

#lg=7.41392994,  bg=51.50361423
class KoordCalculation(): 
    
    longitude = 0.0
    latitude = 0.0
    

    @staticmethod
    def JD():
        x = datetime.utcnow()
        J=float(x.year)
        M=float(x.month)
        T=float(x.day)
        ST=float(x.hour)
        MI=float(x.minute)
        SE=float(x.second)
        
        if(M>2):
            j=J
            m=M
        else:
            j=J-1
            m=M+12

        H = ST/24 + MI/1440 + SE/86400
        
        b = 2 - math.floor (j/100) + math.floor( math.floor (j/100) / 4)
        ret = math.floor(365.25*(j+4716)) + math.floor(30.6001*(m+1)) + T + H + b - 1524.5
        return ret

    @staticmethod
    def SunPosition(): 
        JD = KoordCalculation.JD()
        n = JD - 2451545.0
        L = 280.480 + 0.9856474 * n
        g = 357.528 + 0.9856003 * n
        while(L<0):
            L+=360
        while(L>360):
            L-=360
        while(g<0):
            g+=360
        while(g>360):
            g-=360
        l = L + 1.915 * sin(g) + 0.01997 * sin(2*g)

        e = 23.439 - 0.0000004 * n
        c=cos(l)
        s=sin(l)
        rek=atan((cos(e)*s)/c)
        dek=asin(sin(e)*s)
        if(c<0):
            rek +=180
        return[rek,dek]

    def __init__(self, longitude, latitude):
        self.longitude = longitude
        self.latitude = latitude
    

    def LAST(self):  
        x = datetime.utcnow()
        J=float(x.year)
        M=float(x.month)
        T=float(x.day)
        ST=float(x.hour)
        MI=float(x.minute)
        SE=float(x.second)
        MS=float(x.microsecond)
        
        p = J + (M/100) + (T/10000)
        if p<1582.1015:
            CAL=1
        else:
            CAL=2
        if(M>2): 
            j=J 
            m=M
        else: 
            j=J-1
            m=M+12
        
        jd = math.floor(365.25 * j) + math.floor(30.6001 * (m + 1)) + T + 1720994.5 
        b = 2 - math.floor (j/100) + math.floor( math.floor (j/100) / 4) 
        
        if CAL<2:
            JD=jd
        else:
            JD=jd + b
            
        TN = float(JD - 2451545) / 36525
        GMST0 = (24110.54841 + 8640184.812866 * TN  +  0.093104 * TN * TN  -  0.0000062 * TN * TN * TN) 
        
        GMST0 = GMST0/3600 %24
        
        
        if(GMST0<0): GMST0+=24
            
        d = ST + MI/60 + SE/3600 + MS/3600000000
        GMST = (GMST0 + 1.00273790935 * d) % 24
        LMST = (GMST + self.longitude/15 + 24) % 24 
    
        TE = (JD + d/24 - 2451545.0)/36525
        Omega = 125.04452 - 1934.136261 * TE  +  0.0020708 * TE * TE  +  TE * TE * TE * 1 / 450000
        L = 280.4665 + 36000.7698 * TE
        Lst = 218.3165  +  481267.8813 * TE 
        Depsi = -17.20 * math.sin(math.radians(Omega))  -  1.32 * math.sin(math.radians(2 * L))  -  0.23 * math.sin(math.radians(2 * Lst))  +  0.21 * math.sin ( math.radians(2 * Omega))
        Deps = 9.20 * math.cos(math.radians(Omega))  +  0.57 * math.cos(math.radians(2 * L))  +  0.10 * math.cos(math.radians(2 * Lst))  -  0.09 * math.cos(math.radians( 2 * Omega))  
        eps0 = 23.43929111 + (-46.815 * TE  -  0.00059 * TE * TE  +  0.001813 * TE * TE * TE) / 3600
        eps = eps0 + Deps / 3600
        Depsicoseps =  Depsi * math.cos(math.radians(eps))
        LAST = LMST +  Depsicoseps / (15/ST * 3600)
        if(LAST<0): LAST += 24
    
        return LAST
    
    def PositionAequatorial(self,  az, alt ):
        if(isinstance(az, list)):
            azimut = az[0] + float(az[1])/60 + float(az[2])/3600
        else:
            azimut = az
        if(isinstance(alt, list)):
            altitude = alt[0] + float(alt[1])/60 + float(alt[2])/3600
        else:
            altitude = alt
        last=self.LAST()    
    
        
        tau = math.degrees(math.atan2(math.sin(math.radians((azimut+180)%360)) ,  (math.cos(math.radians((azimut+180)%360)) * math.sin(math.radians(self.latitude)) + math.tan(math.radians(altitude)) * math.cos(math.radians(self.latitude)))))
        tau = (tau/15 + 24) % 24

        deklination = sin(altitude) * sin(self.latitude) - cos(altitude) * cos(self.latitude) * cos((azimut+180)%360)
        deklination = asin(deklination)
        rektaszension = (last-tau+24 ) % 24
                        
        return [rektaszension, deklination]
     
    def PositionHorizontal(self,  ra,  de):
        if(isinstance(ra, list)):
            rektaszension = ra[0] + float(ra[1])/60 + float(ra[2])/3600
        else:
            rektaszension = ra
        if(isinstance(de, list)):
            deklination = de[0] + float(de[1])/60 + float(de[2])/3600
        else:
            deklination = de
        
        last=self.LAST()
        Q = ((last - rektaszension + 24) % 24) * 15
        alt = math.sin(math.radians(self.latitude)) * math.sin(math.radians(deklination)) + math.cos(math.radians(self.latitude)) * math.cos(math.radians(deklination)) * math.cos(math.radians(Q))
        alt = math.degrees(math.asin(alt))
        Zlr = math.sin(math.radians(Q))
        Nnr = (math.cos(math.radians(Q)) * math.sin(math.radians(self.latitude)) - math.tan(math.radians(deklination)) * math.cos(math.radians(self.latitude)))
        az = math.degrees(math.atan(Zlr/Nnr))
        if (Nnr < 0) and (Zlr != 0):
            az = (az+360)%360
        elif (Nnr > 0) and (Zlr < 0):
            az += 180
        elif (Nnr != 0) and (Zlr == 0):
            az=0
        elif (Nnr> 0) and (Zlr > 0):
            az+=180
        return([az, alt])
            
    def isVisible(self, o, min_height=20):
        pos = self.PositionHorizontal(o.ra_h, o.dedeg)
        if(pos[1]>min_height):
            return True
        else:
            return False

    def isVisibleKoord(self, ra_h, dek, min_height=20):
        pos = self.PositionHorizontal(ra_h, dek)
        if(pos[1]>min_height):
            return True
        else:
            return False

    def formatDegrees(self, value):
        if value<0:
            factor = -1
        else:
            factor = 1
        
        v=math.fabs(value)
        d=math.floor(v)
        m=math.floor((v-d)*60)
        s=(v-d-m/60) * 3600
        d*=factor    
        return "{0:1.0f}° {1:1.0f}' {2:1.2f}''".format(d,m,s)

    def formatHours(self, value):
        if value<0:
            factor = -1
        else:
            factor = 1
        
        v=math.fabs(value)
        d=math.floor(v)
        m=math.floor((v-d)*60)
        s=(v-d-m/60) * 3600
        d*=factor    
        return "{0:1.0f}h {1:1.0f}m {2:1.2f}s".format(d,m,s)
        
def sin(v):
    return math.sin(math.radians(v))
def cos(v):
    return math.cos(math.radians(v))
def tan(v):
    return math.tan(math.radians(v))
def asin(v):
    return math.degrees(math.asin(v))
def acos(v):
    return math.degrees(math.acos(v))
def atan(v):
    return math.degrees(math.atan(v))
