# -*- coding: utf-8 -*-
'''
Created on 29.01.2015

@author: martin
'''

from kivy.uix.screenmanager import Screen
from kivy.uix.image import Image
from kivy.properties import StringProperty, ObjectProperty
from kivy.clock import Clock
from time import strftime, localtime
from kivy.uix.label import Label 
import os

class ErrorLabel(Label):
    pass
    
class ScreenMain(Screen):

    uhrzeit = StringProperty(strftime('%H:%M',localtime()))
    temperature = StringProperty("")
    pressure = StringProperty("")
    clockEvent = ObjectProperty()

    app = ObjectProperty()
    errorMessage = ObjectProperty()

    bmp180 = None
    
    start_image = ObjectProperty(Image(source="images/start.png", keep_ratio=True))

    appTitle = StringProperty('DobControl')
    appVersion = StringProperty('Version 1.0')
    appAuthor = StringProperty('by Martin Kotowicz')
    
    def on_enter(self, *args):
        Screen.on_enter(self, *args)
        self.bmp180 = self.app.bmp180
        clockEvent = ObjectProperty()
        self.clockEvent = Clock.schedule_interval(self.updateUhrzeit, 60)
        self.updateUhrzeit(None)
        self.errorMessage.clear_widgets()
        error_count = 0
        if(self.app.nunchuk.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="Nunchuk missing"))
            error_count += 1
        if(self.app.control.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="Control missing"))
            error_count += 1
        if(self.app.oaz.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="OAZ missing"))
            error_count += 1
        if(self.app.bmp180.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="BMP180 missing"))
            error_count += 1
        if(self.app.adxl345.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="ADXL345 missing"))
            error_count += 1
        if(self.app.compass.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="HCM5883L missing"))
            error_count += 1
        if(self.app.at24c32.module_available!=True):
            self.errorMessage.add_widget(ErrorLabel(text="AT24C32 missing"))
            error_count += 1
        self.errorMessage.height = (error_count*25)


    def on_pre_leave(self, *args):
        Screen.on_pre_leave(self, *args)

        if(self.clockEvent):
            Clock.unschedule(self.updateUhrzeit)

    def updateUhrzeit(self, dt):
        bmp_data = self.bmp180.getData()
        self.uhrzeit = strftime('%H:%M',localtime())
        self.temperature = '{}°C'.format(bmp_data["temperature"])
        self.pressure = '{}hPa'.format(bmp_data["pressure"])

    def closeApp(self):
        
        self.app.screenEnd()
