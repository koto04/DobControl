'''
Created on 29.01.2015

@author: martin
'''

from kivy.uix.screenmanager import Screen
from kivy.uix.widget import Widget
from kivy.clock import Clock
from functools import partial
from kivy.properties import ObjectProperty


import CalibrationBall

class CalibrationTarget(Widget):
    pass


class ScreenLevel(Screen):

    app = ObjectProperty()

    adxl345=None
    
    calibration_ball = ObjectProperty(CalibrationBall)
     
    def on_enter(self, *args):
        Screen.on_enter(self, *args)
        self.adxl345 = self.app.adxl345
        Clock.schedule_interval(self.update, 0.2)
    
    def on_pre_leave(self, *args):
        Screen.on_pre_leave(self, *args)
        Clock.unschedule(self.update)
    def update(self, dt):
        values = self.adxl345.getAxes()
        self.calibration_ball.move(values['x'],values['y'])
    def savePlane(self, *args):
        values = self.adxl345.getAxes()
        self.calibration_ball.savePlane(values['x'],values['y'])    
        
