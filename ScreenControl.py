# -*- coding: utf-8 -*-
'''
Created on 29.01.2015

@author: martin
'''

from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.image import Image
from kivy.uix.button import Button 
from kivy.clock import Clock, mainthread
from functools import partial
from time import strftime, localtime
from PopupObjectInfo import PopupObjectInfo
from CatalogObject import CatalogObject

import math
import time
import threading

class ScreenControl(Screen):
    
    rektaszension = StringProperty('not calibrated')
    deklination = StringProperty('not calibrated')
    azimut = StringProperty('not calibrated')
    altitude = StringProperty('not calibrated')
    uhrzeit = StringProperty(strftime('%H:%M',localtime()))
    temperature = StringProperty("")
    pressure = StringProperty("")

    whatThatButtonStatusDisabled = BooleanProperty()
    goPosButtonStatusDisabled = BooleanProperty()
    
    app = ObjectProperty()

    nunchuk = None
    control = None
    calculator = None

    moving = False
    
    nunC = 0
    nunZ = 0

    followRek = 0
    followDek = 0

    isAligned = False    
    following = False
    monitorInput = False
    inMotion = False

    uhrzeitEvent = ObjectProperty()
    displayEvent = ObjectProperty()

    menubar = ObjectProperty()
    menu_page = 1

    updateProcess = None
    
    processLocked = False
    threadLock = False
  
    def __init__(self, **kwargs):
        super(ScreenControl, self).__init__(**kwargs)

        self.bmp180 = self.app.bmp180
        self.nunchuk = self.app.nunchuk
        self.control = self.app.control
        self.oaz = self.app.oaz
        self.calculator = self.app.calculator

        self.buttonCatalog = Button(text="Catalog", id="btnCatalog", on_release=self.app.screenCatalog)
        self.buttonAdjustment = Button(text="Adjustment", id="btnAdjustment", on_release=self.app.screenStar)
        #self.buttonWhatThat = Button(text="What that?", id="btnWhatThat", on_release=self.objectInfo)
        self.buttonStepperSpeed = Button(text="Motor Speed", id="btnStepperSpeed", on_release=self.app.screenStepSpeed)
        self.buttonGoPosition = Button(text="Go to position", id="btnGoPosition", on_release=self.app.screenKoordGoto)       
        self.buttonBack = Button(text="Back", id="btnBack", on_release=self.app.screenMain)
        self.buttonMore = Button(text="More...", id="btnMore", on_release=self.toggleMenu)

        #self.bind(whatThatButtonStatusDisabled=self.buttonWhatThat.setter("disabled"))
        self.bind(goPosButtonStatusDisabled=self.buttonGoPosition.setter("disabled"))
    
    def on_enter(self, *args):

        self.menu_page=0
        self.refreshMenu()
        
        self.updateUhrzeit(None);
        self.displayEvent = Clock.schedule_interval(self.refreshInformation, 0.5)
        self.uhrzeitEvent = Clock.schedule_interval(self.updateUhrzeit, 60)

        if self.app.dobAz<0:
            self.whatThatButtonStatusDisabled=True
            self.goPosButtonStatusDisabled=True
            self.isAligned = False
        else:
            self.whatThatButtonStatusDisabled=False
            self.goPosButtonStatusDisabled=False
            self.isAligned = True

        self.monitorInput = True # monitoring Nunchuk input values in threaded update-function
        self.threadLock = threading.Lock()
        self.updateProcess = threading.Thread(target=self.update)
        self.updateProcess.start()
        
    def on_leave(self, *args):
        self.following = False
        self.monitorInput = False        
        if(self.uhrzeitEvent):
            Clock.unschedule(self.updateUhrzeit)
        if(self.refreshInformation):
            Clock.unschedule(self.refreshInformation)
        self.follow_image.clear_widgets()
        self.updateProcess.join()

    def toggleMenu(self, *args):
        self.menu_page = (self.menu_page +1) % 2
        self.refreshMenu()
        
    def refreshMenu(self, *args):
        self.menubar.clear_widgets()
        if(self.menu_page==0):
            self.menubar.add_widget(self.buttonCatalog)
            self.menubar.add_widget(self.buttonStepperSpeed)
            self.menubar.add_widget(self.buttonMore)
            self.menubar.add_widget(self.buttonBack)
        elif(self.menu_page==1):
            self.menubar.add_widget(self.buttonAdjustment)
            self.menubar.add_widget(self.buttonGoPosition)
            self.menubar.add_widget(self.buttonMore)
            self.menubar.add_widget(self.buttonBack)

    def refreshInformation(self, dt):
        if(self.processLocked==True): return
        #self.threadLock.acquire()
        if(self.isAligned==False):
            self.rektaszension = 'not calibrated'
            self.deklination = 'not calibrated'
            self.azimut = 'not calibrated'
            self.altitude = 'not calibrated'
        else:
            if self.inMotion==False:
                aeqPos = self.app.calculator.PositionAequatorial(self.app.dobAz, self.app.dobAlt)
                self.app.rektaszension = aeqPos[0]
                self.app.deklination = aeqPos[1]
                self.rektaszension = self.app.calculator.formatHours(self.app.rektaszension)
                self.deklination = self.app.calculator.formatDegrees(self.app.deklination)
                self.azimut = self.app.calculator.formatDegrees(self.app.dobAz)
                self.altitude = self.app.calculator.formatDegrees(self.app.dobAlt)
            else:
                self.rektaszension = "----"
                self.deklination = "----"
                self.azimut = "----"
                self.altitude = "----"
                
            
        #self.threadLock.release()
        
    def updatePosition(self, x, y):
        if(self.isAligned==True):
            self.app.dobAz += x
            self.app.dobAlt += y
            if(self.app.dobAz<0): self.app.dobAz+=360
            if(self.app.dobAlt<0): self.app.dobAlt+=360
            if(self.app.dobAz>360): self.app.dobAz-=360
            if(self.app.dobAlt>360): self.app.dobAlt-=360
        
    def updateUhrzeit(self, dt):
        bmp_data = self.bmp180.getData()
        self.uhrzeit = strftime('%H:%M',localtime())
        self.temperature = '{}°C'.format(bmp_data["temperature"])
        self.pressure = '{}hPa'.format(bmp_data["pressure"])


    def update(self):
        
        while(self.monitorInput):
            self.nunchuk.read()
            if(self.isAligned==False):  # Position not calibrated
                if self.controlOAZ()==True: continue
                if self.controlTelescope()==False: time.sleep(0.01)

            else: #telescope is aligned
                if self.nunC != self.nunchuk.c: #start or stop following
                    self.nunC = self.nunchuk.c
                    if self.nunC==1:
                        self.follow()
                    #time.sleep(0.01)
                
                if self.following == True:
                    
                    #Verify moving result of Alt in 0-90 degree range
                    horPos = self.calculator.PositionHorizontal(self.followRek, self.followDek)
                    self.threadLock.acquire()
                    if(horPos[1]>90) or (horPos[1]<0):
                        self.follow()
                        time.sleep(0.001)
                    else:
                        move = self.control.moveDegreeFast((horPos[0] - self.app.dobAz),(horPos[1] - self.app.dobAlt))
                        self.updatePosition(move["x"], move["y"])
                    self.threadLock.release()
                    self.controlOAZ()

                else:
                    if self.controlOAZ()==True: continue
                    if self.controlTelescope()==False: time.sleep(0.01)
 
    def controlOAZ(self):
        ret = True
        if(self.nunchuk.z==1):
            self.nunZ=1
            self.oaz.move(self.nunchuk.y)
        elif(self.nunchuk.z!=self.nunZ): #new z=0, old z=1
            self.oaz.stop()
            self.nunZ=0
        else:
            ret = False
        return ret

    def controlTelescope(self):
        ret = False
        while (self.nunchuk.x!=0 or self.nunchuk.y!=0):
            self.inMotion = True
            ret = True
            move = self.control.moveStep(self.nunchuk.x, self.nunchuk.y)
            self.updatePosition(move["x"], move["y"])
            mx = math.fabs(self.nunchuk.x)
            my = math.fabs(self.nunchuk.y)
            #if((mx==1) and (my<=1)) or ((mx<=1) and (my==1)):
            #   time.sleep(0.05)    
            self.nunchuk.read()
        self.inMotion = False
        return ret
            
    @mainthread    
    def follow(self):
        self.processLocked = True
        self.threadLock.acquire()
        if self.following == False:
            #print "start following"
            self.following = True
            self.followRek = self.app.rektaszension
            self.followDek = self.app.deklination
            img = Image(source="images/following.png", keep_ratio=True)
            img.color = self.app.l_image_color
            self.follow_image.add_widget(img)
            self.control.setFollowUSteps()  # set best step-resolution

        else:
            #print "stop following"
            self.following = False
            self.follow_image.clear_widgets()
            self.control.restoreUstepPreset()
        self.threadLock.release()
        self.processLocked = False
            
    def objectInfo(self, event=None):
        if(self.isAligned==False): return
        o = CatalogObject.findObject(self.app.rektaszension, self.app.deklination)
        p = PopupObjectInfo()
        p.ShowInfo(o)
    
